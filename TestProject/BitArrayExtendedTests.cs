using Common;

namespace TestProject;

public class BitArrayExtendedTests
{
    [Fact]
    public void Test()
    {
        var array = new BitArrayExtended(4096);
        for (var i = 0; i < array.Length; i++)
            Assert.False(array.Get(i));

        var count = array.CountSetBits();
        Assert.Equal(0, count);

        array.Set(12, true);
        Assert.True(array.Get(12));
        Assert.True(array[12]);

        array.SetRange(519, 200, true);
        for (var i = 0; i < 12; i++)
            Assert.False(array[i]);
        Assert.True(array[12]);
        for (var i = 13; i < 519; i++)
            Assert.False(array[i]);
        for (var i = 519; i < 519 + 200; i++)
            Assert.True(array[i]);

        for (var i = 519 + 200; i < array.Length; i++)
            Assert.False(array[i]);

        count = array.CountSetBits();
        Assert.Equal(201, count);

        count = array.CountSetBits(0, 200);
        Assert.Equal(1, count);

        count = array.CountSetBits(500, 100);
        Assert.Equal(100 - 19, count);

        array.SetAll(false);
        for (var i = 0; i < array.Length; i++)
            Assert.False(array.Get(i));

        array.SetRange(1, 2, true);
        Assert.False(array[0]);
        Assert.True(array[1]);
        Assert.True(array[2]);
        Assert.False(array[3]);
        Assert.False(array[4]);

        array.SetAll(false);
        for (var i = 0; i < array.Length; i++)
            Assert.False(array.Get(i));

        array.SetRange(48, 13, true);
        for (var i = 0; i < 48; i++)
            Assert.False(array[i]);
        for (var i = 48; i < 48 + 13; i++)
            Assert.True(array[i]);
        for (var i = 48 + 13; i < array.Length; i++)
            Assert.False(array[i]);
    }

    [Fact]
    public void TestSmall()
    {
        var array = new BitArrayExtended(40);
        for (var i = 0; i < array.Length; i++)
            Assert.False(array.Get(i));

        array.SetAll(true);
        for (var i = 0; i < array.Length; i++)
            Assert.True(array.Get(i));

        array.SetAll(false);
        for (var i = 0; i < array.Length; i++)
            Assert.False(array.Get(i));

        array.SetRange(1, 2, true);
        Assert.False(array[0]);
        Assert.True(array[1]);
        Assert.True(array[2]);
        Assert.False(array[3]);
        Assert.False(array[4]);

        var count = array.CountSetBits();
        Assert.Equal(2, count);

        array.SetAll(false);
        array.SetRange(10, 14, true);
        for (var i = 0; i < 10; i++)
            Assert.False(array[i]);
        for (var i = 10; i < 10 + 14; i++)
            Assert.True(array[i]);
        for (var i = 10 + 14; i < array.Length; i++)
            Assert.False(array[i]);
    }
}