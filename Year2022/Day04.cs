﻿namespace Year2022;

public class Day04 : BaseDay
{
    private struct Range
    {
        public readonly int Start;
        public readonly int End;

        public Range(string start, string end)
        {
            this.Start = int.Parse(start);
            this.End = int.Parse(end);
        }

        public Range(int start, int end)
        {
            this.Start = start;
            this.End = end;
        }

        public static bool HasCompleteOverlap(Range a, Range b)
        {
            return (a.Start <= b.Start && a.End >= b.End)
                || (a.Start >= b.Start && a.End <= b.End);
        }

        public static bool Overlap(Range a, Range b)
        {
            return a.Start <= b.End && b.Start <= a.End;
        }
    }

    private static (long day0, long day1) Parse(string filename)
    {
        var lines = File.ReadAllLines(filename);

        var day0 = 0L;
        var day1 = 0L;
        foreach (var line in lines)
        {
            var firstDash = line.IndexOf('-');
            var comma = line.IndexOf(',', firstDash);
            var secondDash = line.IndexOf('-', comma);
            var start0 = int.Parse(line.AsSpan(0, firstDash));
            var end0 = int.Parse(line.AsSpan(firstDash + 1, comma - 1 - firstDash));
            var start1 = int.Parse(line.AsSpan(comma + 1, secondDash - 1 - comma));
            var end1 = int.Parse(line.AsSpan(secondDash + 1));
            var range0 = new Range(start0, end0);
            var range1 = new Range(start1, end1);

            if (Range.HasCompleteOverlap(range0, range1))
            {
                day0++;
                day1++;
            }
            else if (Range.Overlap(range0, range1))
            {
                day1++;
            }
        }

        return (day0, day1);
    }

    private bool solved;
    private long day0;
    private long day1;

    private void Solve()
    {
        if (this.solved)
        {
            return;
        }

        this.solved = true;

        var (day0, day1) = Parse("data/day04.txt");

        this.day0 = day0;
        this.day1 = day1;
    }

    public override ValueTask<string> Solve_1()
    {
        this.Solve();
        return new ValueTask<string>(this.day0.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        this.Solve();
        return new ValueTask<string>(this.day1.ToString());
    }
}