﻿namespace Year2022;

public class Day02 : BaseDay
{
    private static (char, char)[] Parse(string filename)
    {
        var lines = File.ReadAllLines(filename);
        var result = new (char, char)[lines.Length];
        for (var i = 0; i < lines.Length; i++)
        {
            var line = lines[i];
            result[i] = (line[0], line[2]);
        }

        return result;
    }

    public override ValueTask<string> Solve_1()
    {
        var rounds = Parse("data/day02.txt");

        var score = 0L;
        foreach (var (theirs, mine) in rounds)
        {
            score += mine - 'X' + 1;

            switch (mine)
            {
                // Rock
                case 'X':
                {
                    switch (theirs)
                    {
                        // Rock
                        case 'A':
                            score += 3;
                            break;

                        // Paper
                        case 'B':
                            score += 0;
                            break;

                        // Scissors
                        case 'C':
                            score += 6;
                            break;
                    }

                    break;
                }

                // Paper
                case 'Y':
                {
                    switch (theirs)
                    {
                        // Rock
                        case 'A':
                            score += 6;
                            break;

                        // Paper
                        case 'B':
                            score += 3;
                            break;

                        // Scissors
                        case 'C':
                            score += 0;
                            break;
                    }

                    break;
                }

                // Scissors
                case 'Z':
                {
                    switch (theirs)
                    {
                        // Rock
                        case 'A':
                            score += 0;
                            break;

                        // Paper
                        case 'B':
                            score += 6;
                            break;

                        // Scissors
                        case 'C':
                            score += 3;
                            break;
                    }

                    break;
                }
            }
        }

        return new ValueTask<string>(score.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var rounds = Parse("data/day02.txt");

        var score = 0L;
        foreach (var (theirs, result) in rounds)
        {
            // Do the score for result
            score += (result - 'X') * 3;

            // Figure out score from what we throw.
            switch (result)
            {
                // Lose
                case 'X':
                    switch (theirs)
                    {
                        // Rock
                        case 'A':
                            score += 3;
                            break;

                        // Paper
                        case 'B':
                            score += 1;
                            break;

                        // Scissors
                        case 'C':
                            score += 2;
                            break;
                    }

                    break;

                // Draw
                case 'Y':
                    switch (theirs)
                    {
                        // Rock
                        case 'A':
                            score += 1;
                            break;

                        // Paper
                        case 'B':
                            score += 2;
                            break;

                        // Scissors
                        case 'C':
                            score += 3;
                            break;
                    }

                    break;

                // Win
                case 'Z':
                    switch (theirs)
                    {
                        // Rock
                        case 'A':
                            score += 2;
                            break;

                        // Paper
                        case 'B':
                            score += 3;
                            break;

                        // Scissors
                        case 'C':
                            score += 1;
                            break;
                    }

                    break;
            }
        }

        return new ValueTask<string>(score.ToString());
    }
}