﻿using System.Runtime.CompilerServices;

namespace Year2022;

public class Day12 : BaseDay
{
    private string? filename;
    private Grid? grid;
    private Point end;
    private int solution1;
    private int solution2;

    private void LoadGrid(string filename)
    {
        if (filename == this.filename && this.grid != null)
            return;

        var lines = File.ReadAllLines(filename);
        var width = lines[0].Length;
        var height = lines.Length;
        var grid = new Grid(width, height);
        var start = default(Point);
        var end = default(Point);

        for (var y = 0; y < height; y++)
        {
            var line = lines[y];
            for (var x = 0; x < width; x++)
            {
                var c = line[x];
                switch (c)
                {
                    case 'S':
                    {
                        start = new Point(x, y);
                        break;
                    }
                    case 'E':
                    {
                        end = new Point(x, y);
                        grid[x, y] = 'z' - 'a' + 1 + 1;
                        break;
                    }
                    default:
                    {
                        grid[x, y] = line[x] - 'a' + 1;
                        break;
                    }
                }
            }
        }

        this.grid = grid;
        this.end = end;
        this.filename = filename;
    }

    public override ValueTask<string> Solve_1()
    {
        this.LoadGrid("data/day12.txt");

        this.Search();
        var solutionCount = this.solution1 - 1;
        return new ValueTask<string>(solutionCount.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        this.LoadGrid("data/day12.txt");

        this.Search();
        var solutionCount = this.solution2 - 1;
        return new ValueTask<string>(solutionCount.ToString());
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private bool CanVisit(ref Point point0, ref Point point1)
    {
        var grid = this.grid!;
        var height0 = grid.GetCell(ref point0);
        var height1 = grid.GetCell(ref point1);

        // Keep in mind we're searching backwards now.
        return height0 <= height1 + 1;
    }

    [MethodImpl(MethodImplOptions.AggressiveOptimization)]
    private void Search()
    {
        if (this.solution1 != 0 && this.solution2 != 0)
            return;

        var grid = this.grid!;
        var width = grid.Width;
        var height = grid.Height;

        var pathCount = 1;

        var closed = new BoolGrid(grid.Width, grid.Height);
        var queue = new RingBuffer<Point>(8192);
        queue.Enqueue(this.end);
        var queue1 = new RingBuffer<Point>(queue.Capacity);

        while (!queue.IsEmpty)
        {
            while (queue.TryDequeue(out var lastStep))
            {
                if (closed.GetCell(lastStep))
                    continue;

                var currentHeight = grid.GetCell(ref lastStep);
                if (currentHeight <= 1)
                {
                    if (this.solution2 == 0)
                    {
                        this.solution2 = pathCount;
                    }

                    if (currentHeight == 0)
                    {
                        this.solution1 = pathCount;
                        return;
                    }
                }

                closed.SetCell(lastStep, true);

                var x = lastStep.X;
                var y = lastStep.Y;
                if (x != 0)
                    this.VisitNeighbor(queue1, closed, ref lastStep, x - 1, y);
                if (y != 0)
                    this.VisitNeighbor(queue1, closed, ref lastStep, x, y - 1);

                if (x + 1 < width)
                    this.VisitNeighbor(queue1, closed, ref lastStep, x + 1, y);
                if (y + 1 < height)
                    this.VisitNeighbor(queue1, closed, ref lastStep, x, y + 1);
            }

            pathCount++;
            (queue, queue1) = (queue1, queue);
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
    private void VisitNeighbor(RingBuffer<Point> queue1, BoolGrid closed, ref Point last, int x, int y)
    {
        var next = new Point(x, y);
        if (closed.GetCell(x, y))
            return;

        if (!this.CanVisit(ref last, ref next))
            return;

        queue1.Enqueue(next);
    }
}