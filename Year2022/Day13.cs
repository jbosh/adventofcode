﻿using System.Diagnostics;
using Antlr4.Runtime;

namespace Year2022;

public class Day13 : BaseDay
{
    private List<Node>? nodes;
    private string? filename;

    private void Parse(string filename)
    {
        if (this.filename == filename && this.nodes != null)
            return;

        var lines = File.ReadAllLines(filename);
        var nodes = new List<Node>();

        for (var lineIndex = 0; lineIndex < lines.Length; lineIndex += 3)
        {
            var left = lines[lineIndex + 0];
            var right = lines[lineIndex + 1];

            var leftNode = Day13Visitor.Parse(left);
            var rightNode = Day13Visitor.Parse(right);
            nodes.Add(leftNode);
            nodes.Add(rightNode);
        }

        this.nodes = nodes;
        this.filename = filename;
    }

    public override ValueTask<string> Solve_1()
    {
        this.Parse("data/day13.txt");
        var nodes = this.nodes!;
        var result = 0L;
        var index = 0;
        for (var i = 0; i < nodes.Count; i += 2)
        {
            index++;
            var leftNode = nodes[i + 0];
            var rightNode = nodes[i + 1];
            var comparison = Node.Compare(leftNode, rightNode);
            if (comparison < 0)
            {
                result += index;
            }
        }

        return new ValueTask<string>(result.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        this.Parse("data/day13.txt");

        var nodes = this.nodes!;
        var node0 = Day13Visitor.Parse("[[2]]");
        var node1 = Day13Visitor.Parse("[[6]]");
        nodes.Add(node0);
        nodes.Add(node1);
        nodes.Sort();

        var nodeIndex0 = -1;
        var nodeIndex1 = -1;
        for (var i = 0; i < nodes.Count; i++)
        {
            var node = nodes[i];
            if (ReferenceEquals(node, node0))
                nodeIndex0 = i + 1;
            else if (ReferenceEquals(node, node1))
                nodeIndex1 = i + 1;
        }

        var result = nodeIndex0 * nodeIndex1;
        return new ValueTask<string>(result.ToString());
    }

    public class Node : IComparable, IComparable<Node>
    {
        public readonly List<Node>? Children;
        public readonly uint? Value;
        public Node this[int i] => this.Children![i];

        public override string ToString()
        {
            if (this.Children != null)
                return $"[{string.Join(",", this.Children.Select(c => c.ToString()))}]";
            return this.Value!.ToString()!;
        }

        public Node(uint value)
        {
            this.Value = value;
        }

        public Node(List<Node> children)
        {
            this.Children = children;
        }

        public static bool operator ==(Node left, Node right) => Compare(left, right) == 0;
        public static bool operator !=(Node left, Node right) => Compare(left, right) != 0;
        public static bool operator <(Node left, Node right) => Compare(left, right) == 1;
        public static bool operator <=(Node left, Node right) => Compare(left, right) <= 0;
        public static bool operator >(Node left, Node right) => Compare(left, right) == -1;
        public static bool operator >=(Node left, Node right) => Compare(left, right) >= 0;

        public override bool Equals(object? obj) => this.CompareTo(obj) == 0;

        public override int GetHashCode() => this.Value.GetHashCode();

        public static int Compare(Node left, Node right)
        {
            var isArray = left.Children != null;
            var otherIsArray = right.Children != null;

            if (isArray != otherIsArray)
            {
                if (isArray)
                {
                    var leftChildren = left.Children!;
                    if (leftChildren.Count == 0)
                        return -1;
                    var comparison = Compare(leftChildren[0], right);
                    if (comparison != 0)
                        return comparison;
                    return leftChildren.Count == 1 ? 0 : 1;
                }
                else
                {
                    var rightChildren = right.Children!;
                    if (rightChildren.Count == 0)
                        return 1;
                    var comparison = Compare(left, rightChildren[0]);
                    if (comparison != 0)
                        return comparison;
                    return rightChildren.Count > 1 ? -1 : 0;
                }
            }

            if (isArray)
            {
                var leftChildren = left.Children!;
                var rightChildren = right.Children!;

                var count = Math.Min(leftChildren.Count, rightChildren.Count);
                for (var i = 0; i < count; i++)
                {
                    var comparison = Compare(leftChildren[i], rightChildren[i]);
                    if (comparison != 0)
                        return comparison;
                }

                return leftChildren.Count.CompareTo(rightChildren.Count);
            }

            // Integers
            return left.Value!.Value.CompareTo(right.Value!.Value);
        }

        public int CompareTo(object? obj)
        {
            if (obj is Node node)
                return this.CompareTo(node);
            return 1;
        }

        public int CompareTo(Node? other)
        {
            if (ReferenceEquals(this, other))
                return 0;
            if (ReferenceEquals(null, other))
                return 1;
            return Compare(this, other);
        }
    }

    public class Day13Visitor : Day13BaseVisitor<Node>
    {
        public static Node Parse(string line)
        {
            var inputStream = new AntlrInputStream(line);
            var speakLexer = new Day13Lexer(inputStream);
            var commonTokenStream = new CommonTokenStream(speakLexer);
            var day13Parser = new Day13Parser(commonTokenStream);
            var mainContext = day13Parser.root();
            var visitor = new Day13Visitor();
            var node = visitor.Visit(mainContext);
            return node;
        }

        public override Node VisitSet(Day13Parser.SetContext context)
        {
            if (context.ChildCount == 2)
                return new Node(new List<Node>());

            var children = new List<Node>((context.ChildCount - 1) / 2);
            for (var i = 1; i < context.ChildCount; i += 2)
            {
                children.Add(this.Visit(context.children[i]));
            }

            Debug.Assert((context.ChildCount - 1) / 2 == children.Count, "Didn't set capacity correctly.");

            return new Node(children);
        }

        public override Node VisitNumber(Day13Parser.NumberContext context)
        {
            var text = context.GetText();
            var value = uint.Parse(text);
            return new Node(value);
        }

        public override Node VisitRoot(Day13Parser.RootContext context)
        {
            return this.Visit(context.children[0]);
        }
    }
}