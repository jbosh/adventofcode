using System.Diagnostics;

namespace Year2022;

public class Day08 : BaseDay
{
    private string? filename;
    private byte[][]? allTrees;
    private bool[][]? allVisible;

    private void Parse(string filename)
    {
        if (this.filename == filename && this.allTrees != null && this.allVisible != null)
            return;

        var lines = File.ReadAllLines(filename);
        var width = lines[0].Length;

        var allTrees = new byte[lines.Length][];
        var allVisible = new bool[lines.Length][];
        var verticalTallestTree = new int[width];
        Array.Fill(verticalTallestTree, -1);

        for (var y = 0; y < lines.Length; y++)
        {
            var line = lines[y];
            allTrees[y] = new byte[width];
            allVisible[y] = new bool[width];
            var trees = allTrees[y];
            var visible = allVisible[y];
            var horizontalTallestTree = -1;

            // Parse and search from west and north
            for (var x = 0; x < width; x++)
            {
                var currentTree = line[x] - '0';
                if (currentTree > horizontalTallestTree)
                    visible[x] = true;

                if (currentTree > verticalTallestTree[x])
                    visible[x] = true;

                verticalTallestTree[x] = Math.Max(currentTree, verticalTallestTree[x]);
                horizontalTallestTree = Math.Max(currentTree, horizontalTallestTree);
                trees[x] = (byte)currentTree;
            }

            // Search from east
            horizontalTallestTree = -1;
            for (var x = width - 1; x >= 0; x--)
            {
                var currentTree = trees[x];
                if (currentTree > horizontalTallestTree)
                    visible[x] = true;

                horizontalTallestTree = Math.Max(currentTree, horizontalTallestTree);
            }
        }

        // Search from south
        Array.Fill(verticalTallestTree, -1);
        for (var y = allTrees.Length - 1; y >= 0; y--)
        {
            var trees = allTrees[y];
            var visible = allVisible[y];
            for (var x = 0; x < trees.Length; x++)
            {
                var currentTree = trees[x];

                if (currentTree > verticalTallestTree[x])
                    visible[x] = true;
                verticalTallestTree[x] = Math.Max(currentTree, verticalTallestTree[x]);
            }
        }

        this.allTrees = allTrees;
        this.allVisible = allVisible;
        this.filename = filename;
    }

    public override ValueTask<string> Solve_1()
    {
        this.Parse("data/day08.txt");
        Debug.Assert(this.allVisible != null, "Parse should have initialized this value.");

        var totalVisible = 0;
        for (var y = 0; y < this.allVisible.Length; y++)
        {
            var visible = this.allVisible[y];
            for (var x = 0; x < visible.Length; x++)
            {
                if (visible[x])
                    totalVisible++;
            }
        }

        return new ValueTask<string>(totalVisible.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        this.Parse("data/day08.txt");
        Debug.Assert(this.allVisible != null, "Parse should have initialized this value.");
        Debug.Assert(this.allTrees != null, "Parse should have initialized this value.");

        var tallestTreeScore = 0;
        for (var y = 1; y < this.allTrees.Length - 1; y++)
        {
            var trees = this.allTrees[y];
            var visible = this.allVisible[y];
            for (var x = 1; x < trees.Length - 1; x++)
            {
                if (!visible[x])
                    continue;

                var currentTree = trees[x];
                var searchWest = x - 1;
                for (; searchWest > 0; searchWest--)
                {
                    if (trees[searchWest] >= currentTree)
                        break;
                }

                var searchEast = x + 1;
                for (; searchEast < trees.Length - 1; searchEast++)
                {
                    if (trees[searchEast] >= currentTree)
                        break;
                }

                var searchNorth = y - 1;
                for (; searchNorth > 0; searchNorth--)
                {
                    if (this.allTrees[searchNorth][x] >= currentTree)
                        break;
                }

                var searchSouth = y + 1;
                for (; searchSouth < this.allTrees.Length - 1; searchSouth++)
                {
                    if (this.allTrees[searchSouth][x] >= currentTree)
                        break;
                }

                searchWest = x - searchWest;
                searchEast = searchEast - x;
                searchNorth = y - searchNorth;
                searchSouth = searchSouth - y;
                var score = searchWest * searchEast * searchNorth * searchSouth;
                if (score > tallestTreeScore)
                    tallestTreeScore = score;
            }
        }

        return new ValueTask<string>(tallestTreeScore.ToString());
    }
}