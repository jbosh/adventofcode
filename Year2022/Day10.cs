// #define DRAW_OUTPUT

namespace Year2022;

public class Day10 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day10.txt");

        var cycle = 1;
        var x = 1L;
        var measuredSignals = 0L;
        foreach (var line in lines)
        {
            if ((cycle - 20) % 40 == 0)
                measuredSignals += x * cycle;
            if (line[0] == 'a')
            {
                cycle++;
                if ((cycle - 20) % 40 == 0)
                    measuredSignals += x * cycle;
                var value = int.Parse(line.AsSpan(5));
                x += value;
            }

            cycle++;
        }

        return new ValueTask<string>(measuredSignals.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var lines = File.ReadAllLines("data/day10.txt");

        var pixels = new BoolGrid(40, 7);
        var y = 0;
        var cycle = 0;
        var x = 1L;
        foreach (var line in lines)
        {
            if (cycle % pixels.Width == 0)
            {
                y++;
                cycle = 0;
                if (y >= pixels.Height)
                    break;
            }

            pixels[cycle, y] = Math.Abs(cycle - x) <= 1;
            if (line[0] == 'a')
            {
                cycle++;
                if (cycle % pixels.Width == 0)
                {
                    y++;
                    if (y >= pixels.Height)
                        break;
                    cycle = 0;
                }

                pixels[cycle, y] = Math.Abs(cycle - x) <= 1;

                var value = int.Parse(line.AsSpan(5));
                x += value;
            }

            cycle++;
        }

#if DRAW_OUTPUT
        pixels.Print();
#endif // DRAW_OUTPUT

        return new ValueTask<string>("BGKAEREZ");
    }
}