grammar Day13;

root
    : value EOF
    ;
value
    : number
    | set
    ;
    
number : NUMBER ;
set
    : '[' value (',' value)* ']'
    | '[' ']'
    ;

NUMBER: [0-9]+ ;
