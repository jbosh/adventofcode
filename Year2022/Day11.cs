using System.Diagnostics;
using System.Reflection.Emit;

namespace Year2022;

public class Day11 : BaseDay
{
    private List<Monkey> monkeys = new();

    public override ValueTask<string> Solve_1()
    {
        var monkeys = new List<Monkey>();
        var lines = File.ReadAllLines("data/day11.txt");

        for (var i = 0; i < lines.Length; i += 7)
        {
            Debug.Assert(lines[i + 0] == $"Monkey {monkeys.Count}:", "Monkeys are out of order.");
            var startingItems = new Queue<ulong>(lines[i + 1].Substring("  Starting items: ".Length).Split(',').Select(ulong.Parse));
            var equation = Monkey.ParseEquation(lines[i + 2].Substring("  Operation: new = ".Length));
            var testValue = ulong.Parse(lines[i + 3].Substring("  Test: divisible by ".Length));
            var trueMonkey = int.Parse(lines[i + 4].Substring("    If true: throw to monkey ".Length));
            var falseMonkey = int.Parse(lines[i + 5].Substring("    If false: throw to monkey ".Length));
            Debug.Assert(lines[i + 6] == string.Empty, "Unexpected input.");
            monkeys.Add(new Monkey(monkeys.Count, startingItems, equation, testValue, trueMonkey, falseMonkey));
        }

        for (var round = 0; round < 20; round++)
        {
            foreach (var monkey in monkeys)
            {
                monkey.Inspections += (ulong)monkey.Items.Count;
                while (monkey.Items.TryDequeue(out var item))
                {
                    var newItem = monkey.Operation(item);
                    newItem /= 3;
                    if (newItem % monkey.TestValue == 0)
                    {
                        monkeys[monkey.TrueMonkey].Items.Enqueue(newItem);
                    }
                    else
                    {
                        monkeys[monkey.FalseMonkey].Items.Enqueue(newItem);
                    }
                }
            }
        }

        var topMonkeys = monkeys.OrderByDescending(m => m.Inspections).Take(2).ToArray();

        var monkeyBusiness = topMonkeys[0].Inspections * topMonkeys[1].Inspections;
        return new ValueTask<string>(monkeyBusiness.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var monkeys = new List<Monkey>();
        var lines = File.ReadAllLines("data/day11.txt");

        for (var i = 0; i < lines.Length; i += 7)
        {
            Debug.Assert(lines[i + 0] == $"Monkey {monkeys.Count}:", "Monkeys are out of order.");
            var startingItems = new Queue<ulong>(lines[i + 1].Substring("  Starting items: ".Length).Split(',').Select(ulong.Parse));
            var equation = Monkey.ParseEquation(lines[i + 2].Substring("  Operation: new = ".Length));
            var testValue = ulong.Parse(lines[i + 3].Substring("  Test: divisible by ".Length));
            var trueMonkey = int.Parse(lines[i + 4].Substring("    If true: throw to monkey ".Length));
            var falseMonkey = int.Parse(lines[i + 5].Substring("    If false: throw to monkey ".Length));
            Debug.Assert(lines[i + 6] == string.Empty, "Unexpected input.");
            monkeys.Add(new Monkey(monkeys.Count, startingItems, equation, testValue, trueMonkey, falseMonkey));
        }

        this.monkeys = monkeys;

        var modulo = monkeys.Aggregate(1UL, (acc, monkey) => acc * monkey.TestValue);

        var allItems = monkeys.SelectMany(m => m.Items.Select(i => new { Index = m.Index, Item = i })).ToArray();
        var monkeyItems = allItems
            .AsParallel()
            .Select(o => this.SolveItemPart2(o.Index, o.Item, modulo))
            .ToArray();

        var sum = monkeyItems[0];
        for (var i = 1; i < monkeyItems.Length; i++)
        {
            for (var j = 0; j < sum.Length; j++)
            {
                sum[j] += monkeyItems[i][j];
            }
        }

        var topMonkeys = sum.OrderByDescending(i => i).Take(2).ToArray();
        var monkeyBusiness = topMonkeys[0] * topMonkeys[1];
        return new ValueTask<string>(monkeyBusiness.ToString());
    }

    private ulong[] SolveItemPart2(int index, ulong item, ulong modulo)
    {
        const int RoundCount = 10_000;
        var result = new ulong[this.monkeys.Count];
        var round = 0;
        var currentMonkeyIndex = index;
        var currentMonkey = this.monkeys[index];

        while (round < RoundCount)
        {
            result[currentMonkeyIndex]++;
            var operation = currentMonkey.Operation;
            var newItem = operation(item);
            newItem %= modulo;

            var nextMonkey = newItem % currentMonkey.TestValue == 0
                ? currentMonkey.TrueMonkey
                : currentMonkey.FalseMonkey;
            if (nextMonkey <= currentMonkeyIndex)
                round++;

            item = newItem;
            currentMonkeyIndex = nextMonkey;
            currentMonkey = this.monkeys[currentMonkeyIndex];
        }

        return result;
    }

    private sealed class Monkey
    {
        public readonly MonkeyOperation Operation;
        public readonly ulong TestValue;
        public readonly int TrueMonkey;
        public readonly int FalseMonkey;
        public readonly int Index;
        public readonly Queue<ulong> Items;

        public ulong Inspections;

        public override string ToString() => $"Items: {string.Join(", ", this.Items)}";

        public Monkey(int index, Queue<ulong> items, MonkeyOperation operation, ulong testValue, int trueMonkey, int falseMonkey)
        {
            this.Index = index;
            this.Items = items;
            this.Operation = operation;
            this.TestValue = testValue;
            this.TrueMonkey = trueMonkey;
            this.FalseMonkey = falseMonkey;
        }

        public delegate ulong MonkeyOperation(ulong old);

        private static readonly Dictionary<string, MonkeyOperation> ParsedOperations = new();

        public static MonkeyOperation ParseEquation(string equation)
        {
            if (ParsedOperations.TryGetValue(equation, out var monkeyOperation))
                return monkeyOperation;

            var tokens = equation.Split(' ');
            Debug.Assert(tokens.Length == 3, "Equation too complex.");

            var op0 = tokens[0];
            var op1 = tokens[2];
            var operation = tokens[1];

            var dynamicMethod = new DynamicMethod(string.Empty, typeof(ulong), new[] { typeof(ulong) });
            var ilGen = dynamicMethod.GetILGenerator();

            if (op0 == "old")
                ilGen.Emit(OpCodes.Ldarg_0);
            else
                ilGen.Emit(OpCodes.Ldc_I8, long.Parse(op0));

            if (op1 == "old")
                ilGen.Emit(OpCodes.Ldarg_0);
            else
                ilGen.Emit(OpCodes.Ldc_I8, long.Parse(op1));

            switch (operation[0])
            {
                case '+':
                    ilGen.Emit(OpCodes.Add);
                    break;
                case '*':
                    ilGen.Emit(OpCodes.Mul);
                    break;
            }

            ilGen.Emit(OpCodes.Ret);

            var methodDelegate = (MonkeyOperation)dynamicMethod.CreateDelegate(typeof(MonkeyOperation));
            ParsedOperations.TryAdd(equation, methodDelegate);
            return methodDelegate;
        }
    }
}