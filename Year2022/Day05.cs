﻿namespace Year2022;

public class Day05 : BaseDay
{
    private readonly struct Move
    {
        public readonly int Count;
        public readonly int Source;
        public readonly int Destination;

        public Move(int count, int source, int destination)
        {
            this.Count = count;
            this.Source = source;
            this.Destination = destination;
        }

        public override string ToString() => $"move {this.Count} from {this.Source} to {this.Destination}";
    }

    private List<char>[]? stacks;
    private Move[]? moves;

    private (List<char>[] stacks, Move[] moves) Parse(string filename)
    {
        if (this.stacks != null && this.moves != null)
            return (this.stacks.Select(l => l.ToList()).ToArray(), this.moves);

        var lines = File.ReadAllLines(filename);
        var stackCount = (lines[0].Length + 1) / 4;
        var stacks = new List<char>[stackCount];
        for (var i = 0; i < stacks.Length; i++)
            stacks[i] = new List<char>();

        var lineIndex = 0;
        for (; lineIndex < lines.Length; lineIndex++)
        {
            var line = lines[lineIndex];
            if (line[1] == '1')
            {
                lineIndex += 2;
                break;
            }

            for (var crateIndex = 1; crateIndex < line.Length; crateIndex += 4)
            {
                var c = line[crateIndex];
                if (c != ' ')
                    stacks[(crateIndex - 1) / 4].Add(c);
            }
        }

        // Parse `move (\d+) from (\d+) to (\d+)`
        var moves = new List<Move>();
        for (; lineIndex < lines.Length; lineIndex++)
        {
            var line = lines[lineIndex];
            var moveStart = "move ".Length;
            var moveEnd = line.IndexOf(' ', moveStart);
            var fromStart = moveEnd + " from ".Length;
            var fromEnd = line.IndexOf(' ', fromStart);
            var toStart = fromEnd + " to ".Length;

            var count = int.Parse(line.AsSpan(moveStart, moveEnd - moveStart));
            var source = int.Parse(line.AsSpan(fromStart, fromEnd - fromStart));
            var destination = int.Parse(line.AsSpan(toStart));
            var move = new Move(count, source - 1, destination - 1);
            moves.Add(move);
        }

        for (var i = 0; i < stacks.Length; i++)
        {
            stacks[i].Reverse();
        }

        this.stacks = stacks;
        this.moves = moves.ToArray();
        return (this.stacks.Select(l => l.ToList()).ToArray(), this.moves);
    }

    public override ValueTask<string> Solve_1()
    {
        var (stacks, moves) = this.Parse("data/day05.txt");

        foreach (var move in moves)
        {
            var count = move.Count;
            var source = move.Source;
            var destination = move.Destination;
            for (var i = 0; i < count; i++)
            {
                var crate = stacks[source][^1];
                stacks[source].RemoveAt(stacks[source].Count - 1);
                stacks[destination].Add(crate);
            }
        }

        var tops = new string(stacks.Select(s => s[^1]).ToArray());
        return new ValueTask<string>(tops);
    }

    public override ValueTask<string> Solve_2()
    {
        var (stacks, moves) = this.Parse("data/day05.txt");

        foreach (var move in moves)
        {
            var count = move.Count;
            var source = move.Source;
            var destination = move.Destination;

            // Add
            for (var i = count; i > 0; i--)
            {
                var crate = stacks[source][stacks[source].Count - i];
                stacks[destination].Add(crate);
            }

            // Remove
            for (var i = 0; i < count; i++)
            {
                stacks[source].RemoveAt(stacks[source].Count - 1);
            }
        }

        var tops = new string(stacks.Select(s => s[^1]).ToArray());
        return new ValueTask<string>(tops);
    }
}