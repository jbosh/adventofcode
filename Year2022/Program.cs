﻿using System.Diagnostics;
using System.Reflection;

namespace Year2022;

public static class Program
{
    public static void Main(string[] args)
    {
        var runCount = 1024;
        if (args.Length == 1)
        {
            runCount = int.Parse(args[0]);
        }
        else if (args.Length == 2)
        {
            runCount = int.Parse(args[0]);
            var dayNumber = int.Parse(args[1]);
            ProfileProblem(dayNumber, runCount).GetAwaiter().GetResult();

            return;
        }

#if RELEASE
        WarmProblems(runCount).GetAwaiter().GetResult();
        Solver.SolveAll().Wait();
#else
        Solver.Solve<Day01>().Wait();
#endif
    }

    private static async ValueTask ProfileProblem(int problemNumber, int iterationCount)
    {
        var problems = Assembly.GetEntryAssembly()!.GetTypes()
            .Where(type => typeof(BaseProblem).IsAssignableFrom(type) && !type.IsInterface && !type.IsAbstract)
            .ToArray();
        foreach (var problemType in problems)
        {
            if (problemType.Name == $"Day{problemNumber}")
            {
                var timer = Stopwatch.StartNew();
                for (var iteration = 0; iteration < iterationCount; iteration++)
                {
                    var potentialProblem = Activator.CreateInstance(problemType);
                    if (potentialProblem is BaseProblem problem)
                    {
                        await problem.Solve_1();
                        await problem.Solve_2();
                    }
                }

                timer.Stop();

                Console.WriteLine($"Completed {iterationCount} in {timer.Elapsed}. {(timer.Elapsed / iterationCount).TotalMilliseconds}ms each");
            }
        }

        Solver.Solve(c => c.ClearConsole = false, (uint)problemNumber).Wait();
    }

#if RELEASE
    private static async ValueTask WarmProblems(int count)
    {
        var problems = Assembly.GetEntryAssembly()!.GetTypes()
            .Where(type => typeof(BaseProblem).IsAssignableFrom(type) && !type.IsInterface && !type.IsAbstract)
            .ToArray();
        for (var iteration = 0; iteration < count; iteration++)
        {
            foreach (var problemType in problems)
            {
                var potentialProblem = Activator.CreateInstance(problemType);
                if (potentialProblem is BaseProblem problem)
                {
                    await problem.Solve_1();
                    await problem.Solve_2();
                }
            }
        }
    }
#endif // RELEASE
}