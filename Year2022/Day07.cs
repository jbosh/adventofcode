using System.Diagnostics;
using Common.ListExtensions;

namespace Year2022;

public class Day07 : BaseDay
{
    private string? dataFilename;
    private Dictionary<string, DirectoryNode>? directories;

    private Dictionary<string, DirectoryNode> Parse(string dataFilename)
    {
        if (this.dataFilename == dataFilename && this.directories != null)
            return this.directories;

        this.dataFilename = dataFilename;
        var lines = File.ReadAllLines(dataFilename);
        Debug.Assert(lines[0] == "$ cd /", "Expected first line to go to /.");

        var directoryStack = new List<string>();

        var currentDirectory = new DirectoryNode(string.Empty);
        var directories = new Dictionary<string, DirectoryNode>();
        directories.Add(currentDirectory.FullPath, currentDirectory);

        for (var lineIndex = 1; lineIndex < lines.Length; lineIndex++)
        {
            var line = lines[lineIndex];
            if (line[0] == '$')
            {
                switch (line[2])
                {
                    case 'c':
                    {
                        Debug.Assert(line[3] == 'd', "Unknown command that isn't cd.");

                        var directory = line[5..];
                        if (directory == "..")
                        {
                            directoryStack.Pop();
                            if (directoryStack.Count == 0)
                            {
                                currentDirectory = directories[string.Empty];
                            }
                            else
                            {
                                var fullPath = string.Join('/', directoryStack);
                                currentDirectory = directories[fullPath];
                            }
                        }
                        else
                        {
                            Debug.Assert(directory != "/", "Not implemented jump to root directory.");
                            Debug.Assert(!directory.Contains('/'), "Not implemented multiple directory jumps in single command.");
                            directoryStack.Push(directory);
                            currentDirectory = currentDirectory.GetDirectory(directory);
                        }

                        break;
                    }
                    case 'l':
                    {
                        Debug.Assert(line[3] == 's', "Unknown command that isn't ls.");
                        Debug.Assert(line.Length == 4, "ls shouldn't have arguments.");
                        for (lineIndex++; lineIndex < lines.Length; lineIndex++)
                        {
                            line = lines[lineIndex];
                            if (line[0] == '$')
                            {
                                lineIndex--;
                                break;
                            }

                            if (line[0] == 'd')
                            {
                                Debug.Assert(line.StartsWith("dir "), "Not implemented other type of file.");
                                var path = line.Substring("dir ".Length);

                                directoryStack.Push(path);
                                var fullPath = string.Join('/', directoryStack);
                                directoryStack.Pop();

                                var newDirectory = new DirectoryNode(fullPath);
                                currentDirectory.Add(path, newDirectory);

                                directories.Add(fullPath, newDirectory);
                            }
                            else
                            {
                                var spaceIndex = line.IndexOf(' ');
                                var size = long.Parse(line.Remove(spaceIndex));
                                var path = line[(spaceIndex + 1)..];
                                currentDirectory.AddChild(new FileNode(path, size));
                            }
                        }

                        break;
                    }
                    default:
                    {
                        throw new NotImplementedException();
                    }
                }
            }
        }

        currentDirectory = directories[string.Empty];
        currentDirectory.UpdateSize();

        this.directories = directories;
        return directories;
    }

    public override ValueTask<string> Solve_1()
    {
        var directories = this.Parse("data/day07.txt");
        var result = directories.Values
            .Where(d => d.Size < 100000)
            .Sum(d => d.Size);

        return new ValueTask<string>(result.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var directories = this.Parse("data/day07.txt");
        const long TotalDiskSpace = 70000000L;
        const long RequiredDiskSpace = 30000000L;
        var totalUsedSpace = directories[string.Empty].Size;
        var unusedSpace = TotalDiskSpace - totalUsedSpace;
        var amountToDelete = RequiredDiskSpace - unusedSpace;

        var directorySizeToDelete = directories.Values
            .Select(d => d.Size)
            .Where(s => s > amountToDelete)
            .Min();

        return new ValueTask<string>(directorySizeToDelete.ToString());
    }

    public class DirectoryNode
    {
        public string FullPath { get; }
        public long FileSizes { get; private set; }
        public long Size { get; private set; }

        private readonly Dictionary<string, DirectoryNode> directories = new();
        private readonly Dictionary<string, FileNode> files = new();
        public override string ToString() => $"/{this.FullPath} ({this.Size})";

        public DirectoryNode(string fullPath)
        {
            this.FullPath = fullPath;
        }

        public void Add(string path, DirectoryNode directory)
        {
            this.directories.Add(path, directory);
        }

        public void AddChild(FileNode file)
        {
            this.files.Add(file.Path, file);
            this.FileSizes += file.Size;
        }

        public long UpdateSize()
        {
            var size = 0L;
            foreach (var dir in this.directories.Values)
            {
                size += dir.UpdateSize();
            }

            foreach (var file in this.files.Values)
            {
                size += file.Size;
            }

            this.Size = size;
            return size;
        }

        public DirectoryNode GetDirectory(string path) => this.directories[path];
    }

    public readonly struct FileNode
    {
        public string Path { get; }
        public long Size { get; }
        public override string ToString() => $"{this.Size} {this.Path}";

        public FileNode(string path, long size)
        {
            this.Path = path;
            this.Size = size;
        }
    }
}