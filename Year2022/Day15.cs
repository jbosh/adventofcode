using System.Diagnostics;

namespace Year2022;

public class Day15 : BaseDay
{
    private string? filename;
    private Sensor[]? sensors;
    private Point[]? beacons;

    private void Load(string filename)
    {
        if (this.filename == filename && this.sensors != null)
            return;

        var lines = File.ReadAllLines(filename);
        var sensors = new Sensor[lines.Length];
        var beacons = new HashSet<Point>(lines.Length);
        for (var i = 0; i < lines.Length; i++)
        {
            var line = lines[i].AsSpan();

            line = line.SliceTo('=');
            line = line.SliceTo(',', out var pxSpan);
            line = line.SliceTo('=');
            line = line.SliceTo(':', out var pySpan);
            line = line.SliceTo('=');
            line = line.SliceTo(',', out var bxSpan);
            line = line.SliceTo('=');
            var bySpan = line;
            var px = int.Parse(pxSpan);
            var py = int.Parse(pySpan);
            var bx = int.Parse(bxSpan);
            var by = int.Parse(bySpan);
            sensors[i] = new Sensor(new Point(px, py), new Point(bx, by));

            beacons.Add(new Point(bx, by));
        }

        this.filename = filename;
        this.sensors = sensors;
        this.beacons = beacons.ToArray();
    }

    public override ValueTask<string> Solve_1()
    {
        const int SearchRow = 2_000_000;
        this.Load("data/day15.txt");

        Debug.Assert(this.sensors != null, "Must call load.");

        var segments = this.GetSegmentsPt1(SearchRow);
        var overlapping = Segment.CombineToSegment(segments);

        var result = overlapping.Width;

        Debug.Assert(this.beacons != null, "Must call load.");
        foreach (var beacon in this.beacons)
        {
            if (beacon.Y == SearchRow)
                result--;
        }

        return new ValueTask<string>(result.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        this.Load("data/day15.txt");

        var result = 0L;
        for (var y = 0; y < 4_000_000; y++)
        {
            var segments = this.GetSegmentsPt2(y);
            if (Segment.FindHole(segments, out var holeX))
            {
                result = (holeX * 4000000L) + y;
                break;
            }
        }

        return new ValueTask<string>(result.ToString());
    }

    private List<Segment> GetSegmentsPt1(int row)
    {
        Debug.Assert(this.sensors != null, "Must load first.");
        var segments = new List<Segment>();
        foreach (var sensor in this.sensors)
        {
            if (!sensor.CanDetect(row))
                continue;

            var segment = sensor.GetSegment(row);
            segments.Add(segment);
        }

        return segments;
    }

    private List<Segment> GetSegmentsPt2(int row)
    {
        Debug.Assert(this.sensors != null, "Must load first.");
        var segments = new List<Segment>();
        foreach (var sensor in this.sensors)
        {
            if (!sensor.CanDetect(row))
                continue;

            var segment = sensor.GetSegment(row);
            segment = new Segment(Math.Max(0, segment.Min), Math.Min(segment.Max, 4_000_000));
            segments.Add(segment);
        }

        return segments;
    }

    private struct Sensor
    {
        public Point Position { get; }
        public int X => this.Position.X;
        public int Y => this.Position.Y;
        public int BeaconDistance { get; }
        public override string ToString() => $"Position={this.Position}, Distance={this.BeaconDistance}";

        public Sensor(Point position, Point nearestBeacon)
        {
            this.Position = position;
            this.BeaconDistance = Point.ManhattanDist(ref position, ref nearestBeacon);
        }

        public bool CanDetect(int y)
        {
            var yMin = this.Position.Y - this.BeaconDistance;
            var yMax = this.Position.Y + this.BeaconDistance;
            return yMin <= y && yMax >= y;
        }

        public Segment GetSegment(int y)
        {
            var xDist = this.BeaconDistance - Math.Abs(this.Position.Y - y);
            var xMin = this.X - xDist;
            var xMax = this.X + xDist;

            return new Segment(xMin, xMax);
        }
    }

    private struct Segment : IComparable<Segment>
    {
        public bool Equals(Segment other) => this.Min == other.Min && this.Max == other.Max;
        public override bool Equals(object? obj) => obj is Segment other && this.Equals(other);
        public override int GetHashCode() => this.Min ^ this.Max;

        public int Min { get; }
        public int Max { get; }
        public override string ToString() => $"{this.Min}-{this.Max}";

        public int Width => this.Max - this.Min + 1;

        public Segment(int min, int max)
        {
            this.Min = min;
            this.Max = max;
        }

        public int CompareTo(Segment other) => this.Min.CompareTo(other.Min);
        public static bool operator ==(Segment left, Segment right) => left.Equals(right);
        public static bool operator !=(Segment left, Segment right) => !left.Equals(right);
        public static bool operator <(Segment left, Segment right) => left.CompareTo(right) == 1;
        public static bool operator <=(Segment left, Segment right) => left.CompareTo(right) <= 0;
        public static bool operator >(Segment left, Segment right) => left.CompareTo(right) == -1;
        public static bool operator >=(Segment left, Segment right) => left.CompareTo(right) >= 0;

        public static Segment CombineToSegment(List<Segment> segments)
        {
            segments.Sort();
            var previous = segments[0];

            for (var i = 1; i < segments.Count; i += 1)
            {
                if (previous.Max >= segments[i].Min)
                {
                    previous = new Segment(previous.Min, Math.Max(previous.Max, segments[i].Max));
                }
                else
                {
                    Debug.Assert(false, "Found a second segment.");
                }
            }

            return previous;
        }

        public static bool FindHole(List<Segment> segments, out int hole)
        {
            segments.Sort();
            var previous = segments[0];

            for (var i = 1; i < segments.Count; i += 1)
            {
                var segment = segments[i];
                if (previous.Max >= segment.Min)
                {
                    previous = new Segment(previous.Min, Math.Max(previous.Max, segment.Max));
                }
                else
                {
                    Debug.Assert(false, "Found a second segment.");
                    hole = previous.Max + 1;
                    return true;
                }
            }

            hole = -1;
            return false;
        }
    }
}