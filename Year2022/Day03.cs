﻿using System.Diagnostics;

namespace Year2022;

public class Day03 : BaseDay
{
    private sealed class Rucksack01
    {
        public readonly string Compartment0;
        public readonly string Compartment1;

        public Rucksack01(string line)
        {
            var count = line.Length / 2;
            this.Compartment0 = line.Remove(count);
            this.Compartment1 = line.Substring(count);
            Debug.Assert(line.Length == this.Compartment0.Length + this.Compartment1.Length, "Pack not evenly divisible.");
        }

        public char FindDuplicate()
        {
            for (var i0 = 0; i0 < this.Compartment0.Length; i0++)
            {
                var c0 = this.Compartment0[i0];
                for (var i1 = 0; i1 < this.Compartment1.Length; i1++)
                {
                    var c1 = this.Compartment1[i1];
                    if (c0 == c1)
                        return c0;
                }
            }

            throw new Exception("No duplicates found.");
        }

        public override string ToString() => this.Compartment0 + this.Compartment1;
    }

    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day03.txt");

        var score = 0L;
        foreach (var line in lines)
        {
            var rucksack = new Rucksack01(line);
            var duplicate = rucksack.FindDuplicate();
            if (duplicate < 'a')
                score += duplicate - 'A' + 27;
            else
                score += duplicate - 'a' + 1;
        }

        return new ValueTask<string>(score.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var lines = File.ReadAllLines("data/day03.txt");

        var score = 0L;
        for (var i = 0; i < lines.Length; i += 3)
        {
            var sack0 = lines[i + 0];
            var sack1 = lines[i + 1];
            var sack2 = lines[i + 2];

            var overlap = Intersect(sack0, sack1, sack2);

            if (overlap < 'a')
                score += overlap - 'A' + 27;
            else
                score += overlap - 'a' + 1;
        }

        return new ValueTask<string>(score.ToString());

        static char Intersect(string a, string b, string c)
        {
            for (var i0 = 0; i0 < a.Length; i0++)
            {
                var c0 = a[i0];
                for (var i1 = 0; i1 < b.Length; i1++)
                {
                    var c1 = a[i1];
                    if (c0 != c1)
                        continue;

                    for (var i2 = 0; i2 < c.Length; i2++)
                    {
                        var c2 = a[i2];
                        if (c0 == c2)
                            return c0;
                    }
                }
            }

            return (char)0;
        }
    }
}