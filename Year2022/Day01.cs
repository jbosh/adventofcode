﻿namespace Year2022;

public class Day01 : BaseDay
{
    public class Elf
    {
        public List<int> Items = new();
        public int Sum => this.Items.Sum();

        public override string ToString() => this.Sum.ToString();
    }

    private static List<Elf> Parse(string filename)
    {
        var elves = new List<Elf>();
        var currentElf = new Elf();
        foreach (var line in File.ReadAllLines(filename))
        {
            if (string.IsNullOrWhiteSpace(line))
            {
                elves.Add(currentElf);
                currentElf = new Elf();
            }
            else
            {
                var item = int.Parse(line);
                currentElf.Items.Add(item);
            }
        }

        if (currentElf.Items.Count != 0)
            elves.Add(currentElf);

        return elves;
    }

    public override ValueTask<string> Solve_1()
    {
        var elves = Parse("data/day01.txt");

        var max = elves.Max(e => e.Sum);

        return new ValueTask<string>(max.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var elves = Parse("data/day01.txt");

        var max = elves.OrderByDescending(e => e.Sum).Take(3).Sum(e => e.Sum);

        return new ValueTask<string>(max.ToString());
    }
}