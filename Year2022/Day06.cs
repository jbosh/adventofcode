﻿namespace Year2022;

public class Day06 : BaseDay
{
    private static int Search(int markerSize)
    {
        var input = File.ReadAllText("data/day06.txt");

        for (var markerIndex = markerSize; markerIndex < input.Length; markerIndex++)
        {
            var set = input.AsSpan(markerIndex - markerSize, markerSize);

            var match = false;
            for (var searchIndex = markerSize - 1; searchIndex > 0; searchIndex--)
            {
                var c0 = set[searchIndex];
                for (var testIndex = searchIndex - 1; testIndex >= 0; testIndex--)
                {
                    var c1 = set[testIndex];
                    if (c0 == c1)
                    {
                        match = true;

                        // Jump to earliest point that we could be.
                        markerIndex += testIndex;
                        goto CompleteLoop;
                    }
                }
            }

        CompleteLoop:
            if (!match)
            {
                return markerIndex;
            }
        }

        throw new NotImplementedException();
    }

    public override ValueTask<string> Solve_1()
    {
        var result = Search(4);
        return new ValueTask<string>(result.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var result = Search(14);
        return new ValueTask<string>(result.ToString());
    }
}