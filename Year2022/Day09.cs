namespace Year2022;

public class Day09 : BaseDay
{
    private int knot1;
    private int knot9;

    private void Solve()
    {
        if (this.knot1 != 0 && this.knot9 != 0)
            return;

        var lines = File.ReadAllLines("data/day09.txt");

        const int VisitedLength = 4096;
        var visited1 = new BitArrayExtended(VisitedLength * VisitedLength);
        var visited9 = new BitArrayExtended(VisitedLength * VisitedLength);
        var knots = new Point[10];
        for (var i = 0; i < knots.Length; i++)
            knots[i] = new Point(VisitedLength / 2, VisitedLength / 2);

        var visitedCount1 = 1;
        var visitedCount9 = 1;
        visited1[(VisitedLength / 2 * VisitedLength) + (VisitedLength / 2)] = true;
        visited9[(VisitedLength / 2 * VisitedLength) + (VisitedLength / 2)] = true;

        foreach (var line in lines)
        {
            var direction = line[0];
            var amount = int.Parse(line.AsSpan(2));

            for (var headMovement = 0; headMovement < amount; headMovement++)
            {
                switch (direction)
                {
                    case 'R':
                    {
                        knots[0].X++;
                        break;
                    }
                    case 'U':
                    {
                        knots[0].Y++;
                        break;
                    }
                    case 'L':
                    {
                        knots[0].X--;
                        break;
                    }
                    case 'D':
                    {
                        knots[0].Y--;
                        break;
                    }
                }

                for (var tailIndex = 1; tailIndex < knots.Length; tailIndex++)
                {
                    var head = knots[tailIndex - 1];
                    var tail = knots[tailIndex];
                    var diffX = head.X - tail.X;
                    var diffY = head.Y - tail.Y;
                    if (Math.Abs(diffX) > 1 || Math.Abs(diffY) > 1)
                    {
                        var signX = Math.Sign(diffX);
                        var signY = Math.Sign(diffY);

                        knots[tailIndex].X += signX;
                        knots[tailIndex].Y += signY;
                    }
                    else
                    {
                        // Don't need to simulate any more movement because this knot didn't move.
                        break;
                    }
                }

                if (!visited1[(knots[1].X * VisitedLength) + knots[1].Y])
                {
                    visitedCount1++;
                    visited1[(knots[1].X * VisitedLength) + knots[1].Y] = true;
                }

                if (!visited9[(knots[9].X * VisitedLength) + knots[9].Y])
                {
                    visitedCount9++;
                    visited9[(knots[9].X * VisitedLength) + knots[9].Y] = true;
                }
            }
        }

        this.knot1 = visitedCount1;
        this.knot9 = visitedCount9;
    }

    public override ValueTask<string> Solve_1()
    {
        this.Solve();

        return new ValueTask<string>(this.knot1.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        this.Solve();

        return new ValueTask<string>(this.knot9.ToString());
    }
}