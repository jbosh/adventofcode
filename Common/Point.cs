﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Common;

[SuppressMessage("Minor Code Smell", "S1210:\"Equals\" and the comparison operators should be overridden when implementing \"IComparable\"", Justification = "Comparison operators don't make sense on points.")]
public struct Point : IComparable<Point>
{
    public int X { get; set; }
    public int Y { get; set; }

    public override string ToString() => $"{this.X}, {this.Y}";

    public Point(int x, int y)
    {
        this.X = x;
        this.Y = y;
    }

    public static Point Parse(string line)
    {
        var index = line.IndexOf(',');
        var x = int.Parse(line.Remove(index));
        var y = int.Parse(line.Substring(index + 1));
        return new Point(x, y);
    }

    public static bool operator ==(Point a, Point b) => a.X == b.X && a.Y == b.Y;
    public static bool operator !=(Point a, Point b) => a.X != b.X || a.Y != b.Y;

    public static Point operator +(Point a, Point b) => new(a.X + b.X, a.Y + b.Y);
    public static Point operator -(Point a, Point b) => new(a.X - b.X, a.Y - b.Y);
    public static Point operator -(Point pt) => new(-pt.X, -pt.Y);

    public static int ManhattanDist(ref Point a, ref Point b) => Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y);
    public static int ManhattanDist(Point a, Point b) => Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y);

    public readonly bool Equals(Point other) => this.X == other.X && this.Y == other.Y;
    public readonly override bool Equals(object? obj) => obj is Point other && this.Equals(other);
    public override int GetHashCode() => (this.X << 16) | this.Y;

    public int CompareTo(Point other)
    {
        var xComparison = this.X.CompareTo(other.X);
        if (xComparison != 0)
            return xComparison;
        return this.Y.CompareTo(other.Y);
    }

    public static double DistanceSq(ref Point a, ref Point b)
    {
        var diff = a - b;
        return (diff.X * diff.X) + (diff.Y * diff.Y);
    }

    public static double DistanceSq(Point a, Point b)
    {
        var diff = a - b;
        return (diff.X * diff.X) + (diff.Y * diff.Y);
    }
}