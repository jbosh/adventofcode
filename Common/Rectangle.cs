﻿namespace Common;

public struct Rectangle
{
    public int X;
    public int Y;
    public int Width;
    public int Height;
    public int Bottom => this.Top + this.Height;
    public int Right => this.X + this.Width;
    public int Left => this.X;
    public int Top => this.Y;
    public override string ToString() => $"x={this.X}, y={this.Y}, w={this.Width}, h={this.Height}";

    public Rectangle(int x, int y, int width, int height)
    {
        this.X = x;
        this.Y = y;
        this.Width = width;
        this.Height = height;
    }

    public bool Contains(Point pt)
    {
        return pt.X >= this.X && pt.Y <= this.Y && pt.X <= this.Right && pt.Y >= this.Bottom;
    }
}