﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Common;

public class BoolGrid
{
    public int Width { get; }
    public int Height { get; }
    private readonly BitArrayExtended cells;

    public delegate int SelectDelegate(int x, int y, int value);

    public BoolGrid(int width, int height)
    {
        this.cells = new BitArrayExtended(width * height);
        this.Width = width;
        this.Height = height;
    }

    public bool this[int x, int y]
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        get
        {
            Debug.Assert(x < this.Width, "x < this.Width");
            Debug.Assert(y < this.Height, "x < this.Width");
            return this.cells[(x * this.Height) + y];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        set
        {
            Debug.Assert(x < this.Width, "x < this.Width");
            Debug.Assert(y < this.Height, "y < this.Height");
            this.cells[(x * this.Height) + y] = value;
        }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool GetCell(Point p) => this.GetCell(p.X, p.Y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool GetCell(ref Point p) => this.GetCell(p.X, p.Y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool GetCell(int x, int y)
    {
        return this[x, y];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool GetCell(int x, int y, bool defaultValue)
    {
        if (x < 0 || x >= this.Width || y < 0 || y >= this.Width)
            return defaultValue;
        return this[x, y];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void SetCell(Point pt, bool value) => this.SetCell(pt.X, pt.Y, value);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void SetCell(int x, int y, bool value) => this[x, y] = value;

    public void Print() => this.Print(0, 0, this.Width, this.Height);
    public void Print(int? width, int? height) => this.Print(0, 0, width ?? this.Width, height ?? this.Height);

    public void Print(Rectangle rect) => this.Print(rect.Left, rect.Top, rect.Width, rect.Height);

    public void Print(int left, int top, int width, int height)
    {
        if (Console.WindowHeight < height)
        {
            if (OperatingSystem.IsWindows())
                Console.WindowHeight = height;
            else
                throw new NotSupportedException("Cannot write QR code to terminal that is too small.");
        }

        if (Console.WindowWidth < width)
        {
            if (OperatingSystem.IsWindows())
                Console.WindowWidth = width;
            else
                throw new NotSupportedException("Cannot write QR code to terminal that is too small.");
        }

        width += left;
        height += top;

        for (var y = left; y < height; y += 2)
        {
            for (var x = top; x < width; x++)
            {
                var value0 = this[x, y + 0] ? 1 : 0;
                var value1 = (y + 1 < height)
#pragma warning disable S3358
                    ? (this[x, y + 1] ? 1 : 0)
#pragma warning restore S3358
                    : 0;
                var value = (value0 << 1) | value1;
                switch (value)
                {
                    case 0b00:
                        Console.Write(' ');
                        break;
                    case 0b10:
                        Console.Write('\u2580');
                        break;
                    case 0b01:
                        Console.Write('\u2584');
                        break;
                    case 0b11:
                        Console.Write('\u2588');
                        break;
                }
            }

            Console.WriteLine();
        }
    }
}