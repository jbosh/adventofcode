﻿namespace Common;

public class SortedList<T>
{
    private readonly List<T> list;
    public int Count => this.list.Count;

    public T this[int index]
    {
        get => this.list[index];
        set => this.list[index] = value;
    }

    public SortedList()
    {
        this.list = new List<T>();
    }

    public SortedList(int capacity)
    {
        this.list = new List<T>(capacity);
    }

    public bool Add(T item)
    {
        var index = this.list.BinarySearch(item);
        if (index < 0)
        {
            this.list.Insert(~index, item);
            return true;
        }

        return false;
    }

    public void RemoveAt(int index) => this.list.RemoveAt(index);

    public void Clear() => this.list.Clear();
}