﻿using System.Numerics;
using System.Runtime.CompilerServices;

namespace Common;

public class BitArrayExtended
{
    private readonly ulong[] array;
    public IReadOnlyList<ulong> Array => this.array;
    public int Length { get; }

    public BitArrayExtended(int length)
        : this(length, false)
    {
    }

    public BitArrayExtended(int length, bool defaultValue)
    {
        this.array = new ulong[((length - 1) / 64) + 1];
        this.Length = length;
        if (defaultValue)
            this.SetAll(true);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool Get(int index)
    {
        if (index < 0 || index >= this.Length)
            throw new ArgumentOutOfRangeException(nameof(index));

        return (this.array[index / 64] & (1UL << (index % 64))) != 0;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Set(int index, bool value)
    {
        if (index < 0 || index >= this.Length)
            throw new ArgumentOutOfRangeException(nameof(index));

        if (value)
            this.array[index / 64] |= 1UL << (index % 64);
        else
            this.array[index / 64] &= ~(1UL << (index % 64));
    }

    public void SetAll(bool value)
    {
        var elementValue = value ? ~0UL : 0;
        for (var i = 0; i < this.array.Length; i++)
            this.array[i] = elementValue;
    }

    public void SetRange(int start, int count, bool value)
    {
        var end = start + count;
        var toStart = Math.Min((start + 64) & ~63, end);
        for (var i = start; i < toStart; i++)
        {
            this.Set(i, value);
        }

        if (toStart == end)
        {
            return;
        }

        var toEnd = end & ~63;
        var toStartIndex = toStart >> 6;
        var toEndIndex = toEnd >> 6;
        var intValue = value ? ~0UL : 0;
        for (var i = toStartIndex; i < toEndIndex; i++)
        {
            this.array[i] = intValue;
        }

        for (var i = toEnd; i < end; i++)
        {
            this.Set(i, value);
        }
    }

    public int CountSetBits() => this.CountSetBits(0, this.Length);

    public int CountSetBits(int start, int count)
    {
        var result = 0;
        var end = start + count;
        var toStart = Math.Min((start + 64) & ~63, end);
        for (var i = start; i < toStart; i++)
        {
            result += this.Get(i) ? 1 : 0;
        }

        if (toStart == end)
        {
            return result;
        }

        var toEnd = end & ~63;
        var toStartIndex = toStart >> 6;
        var toEndIndex = toEnd >> 6;
        for (var i = toStartIndex; i < toEndIndex; i++)
        {
            result += BitOperations.PopCount(this.array[i]);
        }

        for (var i = toEnd; i < end; i++)
        {
            result += this.Get(i) ? 1 : 0;
        }

        return result;
    }

    public bool this[int index]
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        get => this.Get(index);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        set => this.Set(index, value);
    }
}