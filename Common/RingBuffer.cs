﻿using System.Diagnostics;

namespace Common;

public class RingBuffer<T>
{
    private readonly T[] buffer;
    private int head;
    private int tail;
    public override string ToString() => $"Count: {this.Count}";

    public RingBuffer(int capacity)
    {
        this.buffer = new T[capacity];
    }

    public void Clear()
    {
        this.head = 0;
        this.tail = 0;
    }

    public int Capacity => this.buffer.Length;

    public int Count
    {
        get
        {
            var diff = this.tail - this.head;
            if (this.head > this.tail)
                return this.buffer.Length + diff;
            return diff;
        }
    }

    public bool TryEnqueue(T value)
    {
        if (this.IsFull)
            return false;

        this.Enqueue(value);
        return true;
    }

    public void Enqueue(T value)
    {
        var next = this.NextItem(this.tail);
        Debug.Assert(next != this.head, "Queue is full.");

        this.buffer[this.tail] = value;
        this.tail = next;
    }

    public bool TryDequeue(out T? outValue)
    {
        if (this.IsEmpty)
        {
            outValue = default;
            return false;
        }

        outValue = this.Dequeue();
        return true;
    }

    public T? Dequeue()
    {
        var next = this.NextItem(this.head);
        Debug.Assert(this.head != this.tail, "Nothing in queue.");

        var result = this.buffer[this.head];
        this.head = next;
        return result;
    }

    public bool TryPeek(out T? outValue)
    {
        if (this.IsEmpty)
        {
            outValue = default;
            return false;
        }

        this.Peek(out outValue);
        return true;
    }

    public void Peek(out T outValue)
    {
        Debug.Assert(this.head != this.tail, "Nothing in queue.");

        outValue = this.buffer[this.head];
    }

    public bool IsEmpty => this.head == this.tail;

    public bool IsFull => this.NextItem(this.tail) == this.head;

    private int NextItem(int idx)
    {
        idx++;
        if (idx == this.buffer.Length)
            idx = 0;
        return idx;
    }
}