﻿namespace Common;

public class Grid
{
    public int Width { get; }
    public int Height { get; }
    public int[] Cells { get; }

    public override string ToString() => $"{this.Width}, {this.Height}";

    public delegate int SelectDelegate(int x, int y, int value);

    public Grid(int width, int height, int[] cells)
    {
        this.Cells = cells;
        this.Width = width;
        this.Height = height;
    }

    public Grid(int width, int height)
        : this(width, height, new int[width * height])
    {
    }

    public int this[int x, int y]
    {
        get => this.Cells[(x * this.Height) + y];
        set => this.Cells[(x * this.Height) + y] = value;
    }

    public static Grid FromFile(string path)
    {
        var lines = File.ReadAllLines(path);
        var height = lines.Length;
        var width = lines[0].Length;

        var cells = new int[width * height];
        for (var x = 0; x < width; x++)
        {
            for (var y = 0; y < height; y++)
            {
                var value = lines[y][x] - '0';
                cells[(x * width) + y] = value;
            }
        }

        return new Grid(width, height, cells);
    }

    public Grid Select(SelectDelegate mutator)
    {
        var cells = new int[this.Width * this.Height];
        for (var x = 0; x < this.Width; x++)
        {
            for (var y = 0; y < this.Height; y++)
            {
                var value = this.Cells[(x * this.Height) + y];
                cells[(x * this.Height) + y] = mutator(x, y, value);
            }
        }

        return new Grid(this.Width, this.Height, cells);
    }

    public void ApplyAll(SelectDelegate mutator)
    {
        for (var x = 0; x < this.Width; x++)
        {
            for (var y = 0; y < this.Height; y++)
            {
                var value = this.Cells[(x * this.Height) + y];
                this.Cells[(x * this.Height) + y] = mutator(x, y, value);
            }
        }
    }

    public int GetCell(ref Point p) => this.GetCell(p.X, p.Y);
    public int GetCell(Point p) => this.GetCell(p.X, p.Y);

    public int GetCell(int x, int y)
    {
        return this.Cells[(x * this.Height) + y];
    }

    public int GetCell(int x, int y, int defaultValue)
    {
        if (x < 0 || x >= this.Width || y < 0 || y >= this.Width)
            return defaultValue;
        return this.Cells[(x * this.Height) + y];
    }

    public void SetCell(int x, int y, int value)
    {
        if (x < 0 || x >= this.Width || y < 0 || y >= this.Width)
            return;
        this.Cells[(x * this.Height) + y] = value;
    }

    public void Clear(int value = 0)
    {
        for (var y = 0; y < this.Height; y++)
        {
            for (var x = 0; x < this.Width; x++)
            {
                this.Cells[(x * this.Height) + y] = value;
            }
        }
    }

    public void Print()
    {
        for (var y = 0; y < this.Height; y++)
        {
            for (var x = 0; x < this.Width; x++)
            {
                var value = this.Cells[(x * this.Height) + y];
                switch (value)
                {
                    case < 0:
                        Console.Write('-');
                        break;
                    case > 26:
                        Console.Write('#');
                        break;
                    default:
                        Console.Write((char)(value + 'a'));
                        break;
                }
            }

            Console.WriteLine();
        }

        Console.WriteLine();
    }

    public static List<Point> AStarSearch()
    {
        throw new NotImplementedException();
    }
}