using System.Diagnostics;

namespace Common;

public static class Extensions
{
    public static ReadOnlySpan<char> SliceTo(this ReadOnlySpan<char> span, char c)
    {
        var index = span.IndexOf(c);
        Debug.Assert(index >= 0, "Could not find c.");
        return span.Slice(index + 1);
    }

    public static ReadOnlySpan<char> SliceTo(this ReadOnlySpan<char> span, char c, out ReadOnlySpan<char> oldSpan)
    {
        var index = span.IndexOf(c);
        Debug.Assert(index >= 0, "Could not find c.");
        oldSpan = span.Slice(0, index);
        return span.Slice(index + 1);
    }
}