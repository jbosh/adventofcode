﻿using System;
using System.Collections.Generic;

namespace Common;

public class GenericComparer<T> : IComparer<T>
{
    private readonly Func<T?, T?, int> func;
    public GenericComparer(Func<T?, T?, int> func)
    {
        this.func = func;
    }

    public int Compare(T? x, T? y) => this.func(x, y);
}