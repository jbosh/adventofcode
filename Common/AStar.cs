﻿using System.Collections;

namespace Common;

public class AStar
{
    public static AStarPath<TNode>? Solve<TNode>(
        Func<TNode, IEnumerable<TNode>> getNeighbors,
        TNode start,
        TNode destination,
        Func<TNode, TNode, double> distance,
        Func<TNode, double> estimate)
    {
        var queue = new PriorityQueue<AStarPath<TNode>, double>();
        queue.Enqueue(new AStarPath<TNode>(start), 0.0);

        return Solve(getNeighbors, queue, destination, distance, estimate);
    }

    public static AStarPath<TNode>? Solve<TNode>(
        Func<TNode, IEnumerable<TNode>> getNeighbors,
        IEnumerable<TNode> starts,
        TNode destination,
        Func<TNode, TNode, double> distance,
        Func<TNode, double> estimate)
    {
        var queue = new PriorityQueue<AStarPath<TNode>, double>();
        foreach (var start in starts)
        {
            queue.Enqueue(new AStarPath<TNode>(start), 0.0);
        }

        return Solve(getNeighbors, queue, destination, distance, estimate);
    }

    private static AStarPath<TNode>? Solve<TNode>(
        Func<TNode, IEnumerable<TNode>> getNeighbors,
        PriorityQueue<AStarPath<TNode>, double> queue,
        TNode destination,
        Func<TNode, TNode, double> distance,
        Func<TNode, double> estimate)
    {
        var closed = new HashSet<TNode>();

        var iterations = 0;
        while (queue.TryDequeue(out var path, out var priority))
        {
            iterations++;
            if (closed.Contains(path.LastStep))
                continue;
            if (path.LastStep?.Equals(destination) == true)
            {
                return path;
            }

            closed.Add(path.LastStep);
            foreach (var n in getNeighbors(path.LastStep))
            {
                var d = distance(path.LastStep, n);
                var newPath = path.AddStep(n, d);
                queue.Enqueue(newPath, newPath.TotalCost + estimate(n));
            }
        }
#pragma warning disable S1168
        return null;
#pragma warning restore S1168
    }

    public class AStarPath<TNode> : IEnumerable<TNode>
    {
        /// <summary>
        /// Gets the last step.
        /// </summary>
        /// <value>The last step.</value>
        public TNode LastStep { get; private set; }

        /// <summary>
        /// Gets the previous steps.
        /// </summary>
        /// <value>The previous steps.</value>
        public AStarPath<TNode>? PreviousSteps { get; private set; }

        /// <summary>
        /// Gets the total cost.
        /// </summary>
        /// <value>The total cost.</value>
        public double TotalCost { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AStarPath{TNode}"/> class.
        /// </summary>
        /// <param name="lastStep">Last step.</param>
        /// <param name="previousSteps">Previous steps.</param>
        /// <param name="totalCost">Total cost.</param>
        private AStarPath(TNode lastStep, AStarPath<TNode>? previousSteps, double totalCost)
        {
            this.LastStep = lastStep;
            this.PreviousSteps = previousSteps;
            this.TotalCost = totalCost;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AStarPath{TNode}"/> class.
        /// </summary>
        /// <param name="start">Start.</param>
        public AStarPath(TNode start)
            : this(start, null, 0)
        {
        }

        /// <summary>
        /// Adds a step to the path.
        /// </summary>
        /// <returns>The new path.</returns>
        /// <param name="step">The step.</param>
        /// <param name="stepCost">The step cost.</param>
        public AStarPath<TNode> AddStep(TNode step, double stepCost)
        {
            return new AStarPath<TNode>(step, this, this.TotalCost + stepCost);
        }

        #region EnumerableImplementation

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public IEnumerator<TNode> GetEnumerator()
        {
            for (var p = this; p != null; p = p.PreviousSteps)
                yield return p.LastStep;
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        #endregion
    }
}