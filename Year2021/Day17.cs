﻿using System.Text.RegularExpressions;

namespace Year2021;

public class Day17 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var targetArea = ParseTargetArea("data/day17.txt");

        var maxVelocity = -targetArea.Bottom - 1;

        var maxHeight = maxVelocity * (maxVelocity + 1) / 2;

        return new ValueTask<string>(maxHeight.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var targetArea = ParseTargetArea("data/day17.txt");
        var maxVelocityY = -targetArea.Bottom - 1;

        var foundVelocity = 0L;

        for (var x = 1; x <= targetArea.Right; x++)
        {
            for (var y = maxVelocityY; y >= targetArea.Bottom; y--)
            {
                var velocity = new Point(x, y);
                var probe = new Point(0, 0);

                while (probe.Y >= targetArea.Bottom && probe.X <= targetArea.Right)
                {
                    if (targetArea.Contains(probe))
                    {
                        foundVelocity++;
                        break;
                    }

                    var newProbe = new Point(probe.X + velocity.X, probe.Y + velocity.Y);
                    var newVelocity = new Point(Math.Max(velocity.X - 1, 0), velocity.Y - 1);
                    probe = newProbe;
                    velocity = newVelocity;
                }
            }
        }

        return new ValueTask<string>(foundVelocity.ToString());
    }

    public static Rectangle ParseTargetArea(string path)
    {
        var text = File.ReadAllText(path);
        var match = Regex.Match(text, @"target area: x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)");
        var x0 = int.Parse(match.Groups[1].Value);
        var x1 = int.Parse(match.Groups[2].Value);
        var y0 = int.Parse(match.Groups[3].Value);
        var y1 = int.Parse(match.Groups[4].Value);
        var left = Math.Min(x0, x1);
        var right = Math.Max(x0, x1);
        var top = Math.Max(y0, y1);
        var bottom = Math.Min(y0, y1);
        return new Rectangle(left, top, right - left, bottom - top);
    }
}