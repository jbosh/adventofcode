﻿using System.Text.RegularExpressions;

namespace Year2021;

public class Day21 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day21.txt");
        var regex = new Regex(@"Player \d starting position: (\d+)");
        var start0 = int.Parse(regex.Match(lines[0]).Groups[1].Value);
        var start1 = int.Parse(regex.Match(lines[1]).Groups[1].Value);

        var score0 = 0;
        var score1 = 0;
        var space0 = start0 - 1;
        var space1 = start1 - 1;
        var dice = 1;
        while (true)
        {
            var totalDice = 0;
            for (var i = 0; i < 3; i++)
            {
                totalDice += dice;
                dice++;
            }

            space0 = (space0 + totalDice) % 10;
            score0 += space0 + 1;
            if (score0 >= 1000)
                break;

            totalDice = 0;
            for (var i = 0; i < 3; i++)
            {
                totalDice += dice;
                dice++;
            }

            space1 = (space1 + totalDice) % 10;
            score1 += space1 + 1;

            if (score1 >= 1000)
                break;
        }

        var diceRolls = dice - 1;
        var output = diceRolls * Math.Min(score0, score1);
        return new ValueTask<string>(output.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        return new ValueTask<string>(string.Empty);
    }
}