﻿namespace Year2021;

public class Day11 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var grid = Grid.FromFile("data/day11.txt");

        const int RoundCount = 100;
        var flashes = 0L;
        for (var round = 0; round < RoundCount; round++)
        {
            var triggered = new Grid(grid.Width, grid.Height);
            grid.ApplyAll((x, y, value) => value + 1);
            var newEnergy = true;

            while (newEnergy)
            {
                newEnergy = false;
                for (var x = 0; x < grid.Width; x++)
                {
                    for (var y = 0; y < grid.Height; y++)
                    {
                        var value = grid[x, y];
                        if (value <= 9)
                            continue;

                        flashes++;
                        triggered[x, y] = 1;
                        grid[x, y] = 0;
                        for (var i = x - 1; i <= x + 1; i++)
                        {
                            for (var j = y - 1; j <= y + 1; j++)
                            {
                                if (triggered.GetCell(i, j, 0) != 0) // triggered means it's going to be 0
                                    continue;

                                var neighborValue = grid.GetCell(i, j, 0);
                                neighborValue++;
                                grid.SetCell(i, j, neighborValue);
                                if (neighborValue >= 9)
                                {
                                    newEnergy = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return new ValueTask<string>(flashes.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var grid = Grid.FromFile("data/day11.txt");

        var triggered = new Grid(grid.Width, grid.Height);

        for (var round = 0; ; round++)
        {
            var flashes = 0L;
            triggered.Clear();
            grid.ApplyAll((x, y, value) => value + 1);
            var newEnergy = true;

            while (newEnergy)
            {
                newEnergy = false;
                for (var x = 0; x < grid.Width; x++)
                {
                    for (var y = 0; y < grid.Height; y++)
                    {
                        var value = grid[x, y];
                        if (value <= 9)
                            continue;

                        flashes++;
                        triggered[x, y] = 1;
                        grid[x, y] = 0;
                        for (var i = x - 1; i <= x + 1; i++)
                        {
                            for (var j = y - 1; j <= y + 1; j++)
                            {
                                if (triggered.GetCell(i, j, 0) != 0) // triggered means it's going to be 0
                                    continue;

                                var neighborValue = grid.GetCell(i, j, 0);
                                neighborValue++;
                                grid.SetCell(i, j, neighborValue);
                                if (neighborValue >= 9)
                                {
                                    newEnergy = true;
                                }
                            }
                        }
                    }
                }
            }

            if (flashes == grid.Width * grid.Height)
            {
                return new ValueTask<string>((round + 1).ToString());
            }
        }
    }
}