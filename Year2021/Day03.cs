﻿namespace Year2021;

public class Day03 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day03.txt");

        var bitCount = lines[0].Length;
        var bits = new int[bitCount][];

        for (var i = 0; i < bits.Length; i++)
            bits[i] = new int[lines.Length];

        for (var lineIndex = 0; lineIndex < lines.Length; lineIndex++)
        {
            var line = lines[lineIndex];
            for (var bitIndex = 0; bitIndex < line.Length; bitIndex++)
            {
                var bit = line[bitIndex];
                bits[bitIndex][lineIndex] = bit == '0' ? 0 : 1;
            }
        }

        var gammaRate = 0UL;
        for (var i = 0; i < bits.Length; i++)
        {
            var oneCount = bits[i].Count(b => b == 1);
            var zeroCount = bits[i].Length - oneCount;
            if (oneCount > zeroCount)
                gammaRate |= 1UL << (bitCount - i - 1);
        }

        var epsilonRate = (~gammaRate) & ((1UL << bitCount) - 1);
        var output = epsilonRate * gammaRate;
        return new ValueTask<string>(output.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var lines = File.ReadAllLines("data/day03.txt");

        var bitCount = lines[0].Length;
        var bits = new int[lines.Length];

        for (var lineIndex = 0; lineIndex < lines.Length; lineIndex++)
        {
            var line = lines[lineIndex];
            var value = 0;
            for (var bitIndex = 0; bitIndex < bitCount; bitIndex++)
            {
                var bit = line[bitIndex] == '0' ? 0 : 1;
                value |= bit << bitIndex;
            }

            bits[lineIndex] = value;
        }

        var query = new List<int>(bits);
        for (var bitIndex = 0; bitIndex < bitCount; bitIndex++)
        {
            var bit = 1 << bitIndex;
            var onesCount = OnesCountAtPosition(query, bitIndex);
            var zeroCount = query.Count - onesCount;
            if (onesCount >= zeroCount)
                query.RemoveAll(b => (b & bit) == 0);
            else
                query.RemoveAll(b => (b & bit) != 0);
            if (query.Count == 1)
                break;
        }

        var oxygenRating = BitReverse(query[0], bitCount);

        query = new List<int>(bits);
        for (var bitIndex = 0; bitIndex < bitCount; bitIndex++)
        {
            var bit = 1 << bitIndex;
            var onesCount = OnesCountAtPosition(query, bitIndex);
            var zeroCount = query.Count - onesCount;
            if (zeroCount > onesCount)
                query.RemoveAll(b => (b & bit) == 0);
            else
                query.RemoveAll(b => (b & bit) != 0);
            if (query.Count == 1)
                break;
        }

        var co2Rating = BitReverse(query[0], bitCount);
        var output = co2Rating * oxygenRating;
        return new ValueTask<string>(output.ToString());

        static int OnesCountAtPosition(IEnumerable<int> bits, int position)
        {
            var bit = 1 << position;
            return bits.Count(b => (b & bit) != 0);
        }

        static int BitReverse(int value, int bitCount)
        {
            var result = 0;
            for (var i = 0; i < bitCount; i++)
            {
                var isSet = (value & (1 << i)) != 0;
                if (isSet)
                    result |= 1 << (bitCount - i - 1);
            }

            return result;
        }
    }
}