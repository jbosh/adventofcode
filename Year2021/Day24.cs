﻿using System.Diagnostics;

namespace Year2021;

public class Day24 : BaseDay
{
    private Solver solver;

    public override ValueTask<string> Solve_1()
    {
        this.solver = new Solver("data/day24.txt");

        return new ValueTask<string>(this.solver.SolveMax().ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        return new ValueTask<string>(this.solver.SolveMin().ToString());
    }

    private sealed class Solver
    {
        private sealed record InputValues(int add0, int add1, int divisor);

        private readonly InputValues[] input;
        private readonly SortedList<long> solutions = new();
        public long Max => this.solutions[^1];
        public long Min => this.solutions[0];

        public Solver(string path)
        {
            this.input = File.ReadAllLines(path)
                .Chunk(18)
                .Select(c => new InputValues(
                    int.Parse(c[5][6..]),
                    int.Parse(c[15][6..]),
                    int.Parse(c[4][6..])))
                .ToArray();
        }

        public void SolveAll()
        {
            this.SolveAll(new int[14], 0, 0);
        }

        private void SolveAll(int[] numbers, int i, int z)
        {
            if (i == 14)
            {
                var solution = 0L;
                var multiplier = 1L;
                for (var digit = numbers.Length - 1; digit >= 0; digit--)
                {
                    solution += numbers[digit] * multiplier;
                    multiplier *= 10;
                }

                this.solutions.Add(solution);
                return;
            }

            var input = this.input[i];
            for (var w = 1; w <= 9; w++)
            {
                numbers[i] = w;
                if (input.add0 < 0)
                {
                    Debug.Assert(input.divisor == 26, "Not implemented divisor that isn't 26.");
                    if ((z % 26) + input.add0 == w)
                    {
                        this.SolveAll(numbers, i + 1, z / 26);
                    }
                    else
                    {
                        // Skip search space because it cannot approach 0
                        Debug.Assert(input.add1 >= 0, "Optimization fails if less than 0.");

                        // this.Solve(i + 1, z + w + input.Add1);
                    }
                }
                else
                {
                    Debug.Assert(input.divisor == 1, "Not implemented divisor that isn't 1.");
                    this.SolveAll(numbers, i + 1, (z * 26) + w + input.add1);
                }
            }
        }

        public long SolveMax()
        {
            return this.SolveMax(new int[14], 0, 0) ?? 0;
        }

        private long? SolveMax(int[] numbers, int i, int z)
        {
            if (i == 14)
            {
                var solution = 0L;
                var multiplier = 1L;
                for (var digit = numbers.Length - 1; digit >= 0; digit--)
                {
                    solution += numbers[digit] * multiplier;
                    multiplier *= 10;
                }

                return solution;
            }

            var input = this.input[i];
            for (var w = 9; w >= 1; w--)
            {
                numbers[i] = w;
                if (input.add0 < 0)
                {
                    Debug.Assert(input.divisor == 26, "Not implemented.");
                    if ((z % 26) + input.add0 == w)
                    {
                        var result = this.SolveMax(numbers, i + 1, z / 26);
                        if (result.HasValue)
                            return result;
                    }
                    else
                    {
                        // Skip search space because it cannot approach 0
                        Debug.Assert(input.add1 >= 0, "Not implemented.");

                        // Solve(i + 1, z + w + input.Add1);
                    }
                }
                else
                {
                    Debug.Assert(input.divisor == 1, "Not implemented.");
                    var result = this.SolveMax(numbers, i + 1, (z * 26) + w + input.add1);
                    if (result.HasValue)
                        return result;
                }
            }

            return null;
        }

        public long SolveMin()
        {
            return this.SolveMin(new int[14], 0, 0) ?? 0;
        }

        private long? SolveMin(int[] numbers, int i, int z)
        {
            if (i == 14)
            {
                var solution = 0L;
                var multiplier = 1L;
                for (var digit = numbers.Length - 1; digit >= 0; digit--)
                {
                    solution += numbers[digit] * multiplier;
                    multiplier *= 10;
                }

                return solution;
            }

            var input = this.input[i];
            for (var w = 1; w <= 9; w++)
            {
                numbers[i] = w;
                if (input.add0 < 0)
                {
                    Debug.Assert(input.divisor == 26, "Not implemented.");
                    if ((z % 26) + input.add0 == w)
                    {
                        var result = this.SolveMin(numbers, i + 1, z / 26);
                        if (result.HasValue)
                            return result;
                    }
                    else
                    {
                        // Skip search space because it cannot approach 0
                        Debug.Assert(input.add1 >= 0, "Not implemented.");

                        // Solve(i + 1, z + w + input.Add1);
                    }
                }
                else
                {
                    Debug.Assert(input.divisor == 1, "Not implemented.");
                    var result = this.SolveMin(numbers, i + 1, (z * 26) + w + input.add1);
                    if (result.HasValue)
                        return result;
                }
            }

            return null;
        }
    }
}