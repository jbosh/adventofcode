﻿namespace Year2021;

public class Day02 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day02.txt");

        var x = 0;
        var depth = 0;

        foreach (var line in lines)
        {
            var splits = line.Split(' ', 2);
            var cmd = splits[0];
            var amt = int.Parse(splits[1]);
            switch (cmd)
            {
                case "forward":
                    x += amt;
                    break;
                case "backward":
                    x -= amt;
                    break;
                case "up":
                    depth -= amt;
                    break;
                case "down":
                    depth += amt;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        var output = x * depth;
        return new ValueTask<string>(output.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var lines = File.ReadAllLines("data/day02.txt");

        var x = 0;
        var depth = 0;
        var aim = 0;

        foreach (var line in lines)
        {
            var splits = line.Split(' ', 2);
            var cmd = splits[0];
            var amt = int.Parse(splits[1]);
            switch (cmd)
            {
                case "forward":
                    x += amt;
                    depth += aim * amt;
                    break;
                case "backward":
                    x -= amt;
                    depth -= aim * amt;
                    break;
                case "up":
                    aim -= amt;
                    break;
                case "down":
                    aim += amt;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        var output = x * depth;
        return new ValueTask<string>(output.ToString());
    }
}