﻿namespace Year2021;

public static class Program
{
    public static void Main()
    {
#if DEBUG
#if RELEASE
        Solver.SolveAll().Wait();
        Console.Clear();
        Solver.SolveAll().Wait();
#else
        Solver.Solve<Day24>().Wait();
#endif
#else
        for (var i = 0; i < 10; i++)
        {
            var day = new Day19();
            day.Solve_1().GetAwaiter().GetResult();
            day.Solve_2().GetAwaiter().GetResult();
        }
#endif
    }
}
