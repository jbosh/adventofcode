﻿using System.Diagnostics;

namespace Year2021;

public class Day16 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var reader = BitReader.FromHexFile("data/day16.txt");

        var output = 0L;
        var packet = Packet.Parse(reader);
        output += AddVersions(packet);

        return new ValueTask<string>(output.ToString());

        static long AddVersions(Packet packet)
        {
            var result = (long)packet.Version;
            if (packet.Children != null)
            {
                foreach (var child in packet.Children)
                    result += AddVersions(child);
            }

            return result;
        }
    }

    public override ValueTask<string> Solve_2()
    {
        var reader = BitReader.FromHexFile("data/day16.txt");

        var packet = Packet.Parse(reader);

        var output = packet.ComputedValue;
        return new ValueTask<string>(output.ToString());
    }

    public class Packet
    {
        public enum PacketType
        {
            Sum = 0,
            Product,
            Min,
            Max,
            Literal,
            Greater,
            Less,
            Equal,
        }

        public List<Packet> Children;
        public long Value;
        public PacketType ID;
        public int Version;
        public override string ToString() => this.Children == null ? $"{this.Value} [{this.ID}]" : $"{this.ComputedValue} (Count = {this.Children.Count}) [{this.ID}]";

        public static Packet Parse(BitReader reader)
        {
            var packet = new Packet();
            var version = reader.ReadBits(3);
            var id = (PacketType)reader.ReadBits(3);
            if (id == PacketType.Literal)
            {
                var value = reader.ReadLiteral();
                packet.Value = value;
            }
            else
            {
                var children = new List<Packet>();
                var lengthType = reader.ReadBit();
                if (lengthType)
                {
                    // subpackets
                    var packetCount = reader.ReadBits(11);
                    for (var i = 0; i < packetCount; i++)
                    {
                        var child = Parse(reader);
                        children.Add(child);
                    }
                }
                else
                {
                    // bits
                    var bitCount = reader.ReadBits(15);
                    var end = reader.Count - bitCount;
                    while (reader.Count != end)
                    {
                        var child = Parse(reader);
                        children.Add(child);
                    }
                }

                packet.Children = children;
            }

            packet.Version = (int)version;
            packet.ID = id;

            return packet;
        }

        public long ComputedValue
        {
            get
            {
                var value = this.ID switch
                {
                    PacketType.Sum => this.Children.Sum(c => c.ComputedValue),
                    PacketType.Product => this.Children.Aggregate(1L, (current, p) => current * p.ComputedValue),
                    PacketType.Min => this.Children.Min(c => c.ComputedValue),
                    PacketType.Max => this.Children.Max(c => c.ComputedValue),
                    PacketType.Literal => this.Value,
                    PacketType.Greater => this.Children[0].ComputedValue > this.Children[1].ComputedValue ? 1 : 0,
                    PacketType.Less => this.Children[0].ComputedValue < this.Children[1].ComputedValue ? 1 : 0,
                    PacketType.Equal => this.Children[0].ComputedValue == this.Children[1].ComputedValue ? 1 : 0,
                    _ => throw new NotImplementedException(),
                };

                return value;
            }
        }
    }

    public class BitReader
    {
        private readonly Queue<bool> queue;
        public int Count => this.queue.Count;
        public override string ToString() => $"Count = {this.Count}";

        private BitReader(int capacity)
        {
            this.queue = new Queue<bool>(capacity);
        }

        public static BitReader FromHexFile(string path) => FromHex(File.ReadAllText(path));

        public static BitReader FromHex(string hex)
        {
            var result = new BitReader(hex.Length * 8);
            foreach (var c in hex)
            {
                Debug.Assert((c >= '0' && c <= '9') || (c >= 'A' && c <= 'F'), "Invalid hex character.");
                byte value;
                if (c <= '9')
                    value = (byte)(c - '0');
                else
                    value = (byte)(c - 'A' + 10);

                for (var bitIndex = 3; bitIndex >= 0; bitIndex--)
                {
                    var bit = (value & (1 << bitIndex)) != 0;
                    result.queue.Enqueue(bit);
                }
            }

            return result;
        }

        public uint ReadBits(int count)
        {
            var result = 0u;
            for (var i = count - 1; i >= 0; i--)
            {
                var bit = this.queue.Dequeue() ? 1u : 0u;
                result |= bit << i;
            }

            return result;
        }

        public bool ReadBit() => this.queue.Dequeue();

        public long ReadLiteral()
        {
            var value = 0UL;
            while (true)
            {
                var more = this.ReadBit();
                var block = this.ReadBits(4);
                value = (value << 4) | block;
                if (!more)
                    break;
            }

            return checked((long)value);
        }

        public void Print()
        {
            foreach (var b in this.queue)
                Console.Write(b ? '1' : '0');
        }
    }
}