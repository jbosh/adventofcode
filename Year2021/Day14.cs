﻿namespace Year2021;

public class Day14 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        return new ValueTask<string>(Solve(10).ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        return new ValueTask<string>(Solve(40).ToString());
    }

    private static ulong Solve(int iterations)
    {
        var lines = File.ReadAllLines("data/day14.txt");
        var template = lines[0];

        var rules = new byte[KeySpace];
        foreach (var line in lines.Skip(2))
        {
            var key = GetKey(line, 0);
            var value = GetValue(line, "## -> ".Length);
            rules[key] = value;
        }

        var bins0 = new ulong[KeySpace];
        for (var i = 0; i < template.Length - 1; i++)
        {
            var key = GetKey(template, i);
            bins0[key]++;
        }

        var bins1 = new ulong[KeySpace];
        for (var iteration = 0; iteration < iterations; iteration++)
        {
            Array.Clear(bins1);
            for (var key = (ushort)0; key < bins0.Length; key++)
            {
                var bin = bins0[key];
                if (bin == 0)
                    continue;

                var newValue = rules[key];
                var key0 = (key & KeyMask) | (newValue << KeyShift);
                var key1 = (key & (KeyMask << KeyShift)) | newValue;
                bins1[key0] += bin;
                bins1[key1] += bin;
            }

            (bins1, bins0) = (bins0, bins1);
        }

        var characterCounts = new ulong[1 << KeyShift];
        for (var key = (ushort)0; key < bins0.Length; key++)
        {
            var value = bins0[key];
            if (value == 0)
                continue;

            var key0 = key & KeyMask;
            var key1 = key >> KeyShift;
            characterCounts[key0] += value;
            characterCounts[key1] += value;
        }

        for (var i = 0; i < characterCounts.Length; i++)
        {
            // Divide by 2 because every character is counted twice
            characterCounts[i] >>= 1;
        }

        // Add 1 to character at start and end because they will have lost it in division.
        characterCounts[GetValue(template[0])]++;
        characterCounts[GetValue(template[^1])]++;

        var leastCommon = (key: 'A', value: ulong.MaxValue);
        var mostCommon = (key: 'A', value: 0UL);
        for (var character = 0; character < characterCounts.Length; character++)
        {
            var count = characterCounts[character];
            if (count == 0)
                continue;
            if (count < leastCommon.value)
            {
                leastCommon = (key: GetCharFromValue(character), value: count);
            }

            if (count > mostCommon.value)
            {
                mostCommon = (key: GetCharFromValue(character), value: count);
            }
        }

        var output = mostCommon.value - leastCommon.value;
        return output;
    }

    private const int KeySpace = 1 << (KeyShift * 2); // 5 bits per character
    private const int KeyMask = (1 << KeyShift) - 1;
    private const int KeyShift = 5;

    private static ushort GetKey(string s, int offset)
    {
        var top = GetValue(s, offset + 0);
        var bottom = GetValue(s, offset + 1);
        return (ushort)((top << 5) | bottom);
    }

    private static byte GetValue(string s, int offset) => GetValue(s[offset]);
    private static byte GetValue(char c) => (byte)(c - 'A');

    private static string GetStringFromKey(ushort key)
    {
        Span<char> chars = stackalloc char[2];
        chars[0] = GetCharFromValue(key >> KeyShift);
        chars[1] = GetCharFromValue(key & KeyMask);
        return new string(chars);
    }

    private static char GetCharFromValue(int value) => (char)(value + 'A');

    private static void Swap<T>(ref T a, ref T b)
        where T : class
    {
        (a, b) = (b, a);
    }
}