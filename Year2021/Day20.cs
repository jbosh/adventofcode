﻿using System.Diagnostics;

namespace Year2021;

public class Day20 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var litPixels = Solve("data/day20.txt", 2);
        return new ValueTask<string>(litPixels.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var litPixels = Solve("data/day20.txt", 50);
        return new ValueTask<string>(litPixels.ToString());
    }

    private static int Solve(string path, int iterations)
    {
        var lines = File.ReadAllLines(path);
        var filter = lines[0].Select(c => c == '#').ToArray();
        Debug.Assert(filter.Length == 512, "If filter length changes, some optimizations may not work.");

        const int k_gridSize = 8192;
        var inputGrid = new BoolGrid(k_gridSize, k_gridSize);
        var outputGrid = new BoolGrid(k_gridSize, k_gridSize);
        var minX = k_gridSize / 2;
        var minY = k_gridSize / 2;
        var maxX = minX + lines[2].Length;
        var maxY = minY + lines.Length - 2;
        var bounds = new Rectangle(minX, minY, maxX - minX, maxY - minY);
        for (var y = 0; y < lines.Length - 2; y++)
        {
            var line = lines[y + 2];
            for (var x = 0; x < line.Length; x++)
            {
                inputGrid[x + (k_gridSize / 2), y + (k_gridSize / 2)] = line[x] == '#';
            }
        }

        var backgroundPixels = false;

        for (var i = 0; i < iterations; i++)
        {
            Step(ref bounds, ref inputGrid, ref outputGrid, filter, backgroundPixels);

            if (filter[0])
                backgroundPixels = !backgroundPixels;
        }

        var litPixels = 0;
        for (var x = bounds.Left - 1; x <= bounds.Right + 1; x++)
        {
            for (var y = bounds.Top - 1; y <= bounds.Bottom + 1; y++)
            {
                if (inputGrid[x, y])
                    litPixels++;
            }
        }

        return litPixels;
    }

    private static void Step(ref Rectangle bounds, ref BoolGrid input, ref BoolGrid output, bool[] filter, bool backgroundPixels)
    {
        for (var x = bounds.Left - 1; x < bounds.Right + 1; x++)
        {
            for (var y = bounds.Top - 1; y < bounds.Bottom + 1; y++)
            {
                var bit0 = GetBit(input, bounds, x - 1, y - 1);
                var bit1 = GetBit(input, bounds, x + 0, y - 1);
                var bit2 = GetBit(input, bounds, x + 1, y - 1);
                var bit3 = GetBit(input, bounds, x - 1, y + 0);
                var bit4 = GetBit(input, bounds, x + 0, y + 0);
                var bit5 = GetBit(input, bounds, x + 1, y + 0);
                var bit6 = GetBit(input, bounds, x - 1, y + 1);
                var bit7 = GetBit(input, bounds, x + 0, y + 1);
                var bit8 = GetBit(input, bounds, x + 1, y + 1);

                var value = (bit0 << 8) | (bit1 << 7) | (bit2 << 6)
                    | (bit3 << 5) | (bit4 << 4) | (bit5 << 3)
                    | (bit6 << 2) | (bit7 << 1) | (bit8 << 0);

                var newValue = filter[value];
                output[x, y] = newValue;
            }
        }

        bounds = new Rectangle(bounds.X - 1, bounds.Y - 1, bounds.Width + 2, bounds.Height + 2);
        (input, output) = (output, input);

        int GetBit(BoolGrid input, Rectangle bounds, int x, int y)
        {
            if (x < bounds.Left || y < bounds.Top || x >= bounds.Right || y >= bounds.Bottom)
                return backgroundPixels ? 1 : 0;
            return input[x, y] ? 1 : 0;
        }
    }
}