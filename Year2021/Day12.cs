﻿namespace Year2021;

public class Day12 : BaseDay
{
    private long pathCount;
    private byte[] visitCount;
    private bool visitedSmallCave;

    public override ValueTask<string> Solve_1()
    {
        var start = Node.LoadGraph("data/day12.txt", out var nodeCount);

        this.pathCount = 0;
        this.visitCount = new byte[nodeCount];
        this.VisitNodePart1(start);
        return new ValueTask<string>(this.pathCount.ToString());
    }

    private void VisitNodePart1(Node node)
    {
        // only end node has 0 connections
        if (node.Connections.Count == 0)
        {
            this.pathCount++;
        }
        else
        {
            foreach (var connection in node.Connections)
            {
                if (connection.IsBig)
                {
                    this.VisitNodePart1(connection);
                }
                else if (this.visitCount[connection.Index] == 0)
                {
                    this.visitCount[connection.Index]++;
                    this.VisitNodePart1(connection);
                    this.visitCount[connection.Index]--;
                }
            }
        }
    }

    public override ValueTask<string> Solve_2()
    {
        var start = Node.LoadGraph("data/day12.txt", out var nodeCount);

        this.pathCount = 0;
        this.visitCount = new byte[nodeCount];
        this.visitedSmallCave = false;
        this.VisitNodePart2(start);
        return new ValueTask<string>(this.pathCount.ToString());
    }

    private void VisitNodePart2(Node node)
    {
        // only end node has 0 connections
        if (node.Connections.Count == 0)
        {
            this.pathCount++;
        }
        else
        {
            foreach (var connection in node.Connections)
            {
                if (connection.IsBig)
                {
                    this.VisitNodePart2(connection);
                }
                else if (this.visitCount[connection.Index] == 0)
                {
                    this.visitCount[connection.Index]++;
                    this.VisitNodePart2(connection);
                    this.visitCount[connection.Index]--;
                }
                else if (!this.visitedSmallCave && this.visitCount[connection.Index] == 1)
                {
                    this.visitedSmallCave = true;
                    this.visitCount[connection.Index]++;
                    this.VisitNodePart2(connection);
                    this.visitedSmallCave = false;
                    this.visitCount[connection.Index]--;
                }
            }
        }
    }

    private sealed class Node
    {
        public readonly int Index;
        public readonly bool IsBig;
        public readonly List<Node> Connections = new();
        private readonly string name;
        public override string ToString() => $"{this.name}: {this.Connections.Count} connections";

        private Node(string name, int index, bool isBig)
        {
            this.name = name;
            this.Index = index;
            this.IsBig = isBig;
        }

        public static Node LoadGraph(string path, out int nodeCount)
        {
            var lines = File.ReadAllLines(path);

            var lookup = new Dictionary<string, int>
            {
                ["start"] = 0,
                ["end"] = 1,
            };

            var start = new Node("start", 0, false);
            var end = new Node("end", 1, false);
            var nodes = new List<Node>
            {
                start,
                end,
            };

            foreach (var line in lines)
            {
                var names = line.Split('-');
                Node nodeA, nodeB;
                if (lookup.TryGetValue(names[0], out var nodeAIndex))
                {
                    nodeA = nodes[nodeAIndex];
                }
                else
                {
                    lookup[names[0]] = nodes.Count;
                    nodeA = new Node(names[0], nodes.Count, char.IsUpper(names[0][0]));
                    nodes.Add(nodeA);
                }

                if (lookup.TryGetValue(names[1], out var nodeBIndex))
                {
                    nodeB = nodes[nodeBIndex];
                }
                else
                {
                    lookup[names[1]] = nodes.Count;
                    nodeB = new Node(names[1], nodes.Count, char.IsUpper(names[1][0]));
                    nodes.Add(nodeB);
                }

                // Add connections, don't allow start to show up in graph
                if (!ReferenceEquals(nodeB, start))
                    nodeA.Connections.Add(nodeB);
                if (!ReferenceEquals(nodeA, start))
                    nodeB.Connections.Add(nodeA);
            }

            end.Connections.Clear();
            nodeCount = nodes.Count;
            return nodes[0];
        }
    }
}