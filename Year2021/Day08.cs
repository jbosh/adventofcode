﻿using System.Diagnostics;

namespace Year2021;

public class Day08 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day08.txt");

        var totalCount = 0;
        foreach (var line in lines)
        {
            var (digits, displays) = ParseLine(line);
            var foundDigits = new string[10];
            foreach (var digit in digits)
            {
                switch (digit.Length)
                {
                    case 2:
                        Debug.Assert(foundDigits[1] == null, "Cannot set digit multiple times.");
                        foundDigits[1] = digit;
                        break;
                    case 4:
                        Debug.Assert(foundDigits[4] == null, "Cannot set digit multiple times.");
                        foundDigits[4] = digit;
                        break;
                    case 3:
                        Debug.Assert(foundDigits[7] == null, "Cannot set digit multiple times.");
                        foundDigits[7] = digit;
                        break;
                    case 7:
                        Debug.Assert(foundDigits[8] == null, "Cannot set digit multiple times.");
                        foundDigits[8] = digit;
                        break;
                }
            }

            var count = 0;
            foreach (var display in displays)
            {
                if (display == foundDigits[1]
                    || display == foundDigits[4]
                    || display == foundDigits[7]
                    || display == foundDigits[8])
                {
                    count++;
                }
            }

            totalCount += count;
        }

        return new ValueTask<string>(totalCount.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var lines = File.ReadAllLines("data/day08.txt");

        var totalSum = 0L;
        foreach (var line in lines)
        {
            var (digits, displays) = ParseLine(line);
            var foundDigits = new string[10];
            foreach (var digit in digits)
            {
                switch (digit.Length)
                {
                    case 2:
                        Debug.Assert(foundDigits[1] == null, "Cannot set digit multiple times.");
                        foundDigits[1] = digit;
                        break;
                    case 4:
                        Debug.Assert(foundDigits[4] == null, "Cannot set digit multiple times.");
                        foundDigits[4] = digit;
                        break;
                    case 3:
                        Debug.Assert(foundDigits[7] == null, "Cannot set digit multiple times.");
                        foundDigits[7] = digit;
                        break;
                    case 7:
                        Debug.Assert(foundDigits[8] == null, "Cannot set digit multiple times.");
                        foundDigits[8] = digit;
                        break;
                }
            }

            //  0000
            // 1    2
            // 1    2
            //  3333
            // 4    5
            // 4    5
            //  6666
            var positions = new char[7];

            var characterCounts = new int[7];
            foreach (var d in digits)
            {
                foreach (var c in d)
                    characterCounts[c - 'a']++;
            }

            positions[1] = (char)(IndexOf(characterCounts, p => p == 6) + 'a');
            positions[4] = (char)(IndexOf(characterCounts, p => p == 4) + 'a');
            positions[5] = (char)(IndexOf(characterCounts, p => p == 9) + 'a');

            // 0000 and 22 have same count, figure out which one is in the #1
            for (var key = 0; key < characterCounts.Length; key++)
            {
                var p = characterCounts[key];
                if (p != 8)
                    continue;

                if (foundDigits[1].Contains((char)(key + 'a')))
                    positions[2] = (char)(key + 'a');
                else
                    positions[0] = (char)(key + 'a');
            }

            // 3333 and 6666 have same count, figure out which one is in the #4
            for (var key = 0; key < characterCounts.Length; key++)
            {
                var p = characterCounts[key];
                if (p != 7)
                    continue;

                if (foundDigits[4].Contains((char)(key + 'a')))
                    positions[3] = (char)(key + 'a');
                else
                    positions[6] = (char)(key + 'a');
            }

            foundDigits[0] = Construct(0, 1, 2, 4, 5, 6);
            foundDigits[2] = Construct(0, 2, 3, 4, 6);
            foundDigits[3] = Construct(0, 2, 3, 5, 6);
            foundDigits[5] = Construct(0, 1, 3, 5, 6);
            foundDigits[6] = Construct(0, 1, 3, 4, 5, 6);
            foundDigits[9] = Construct(0, 1, 2, 3, 5, 6);

            // Example solution:
            // acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf
            // 0: abcdeg
            // 1: ab
            // 2: acdfg
            // 3: abcdf
            // 4: abef
            // 5: bcdef
            // 6: bcdefg
            // 7: abd
            // 8: abcdefg
            // 9: abcdef

            var values = new List<int>();
            foreach (var display in displays)
            {
                var value = IndexOf(foundDigits, d => d == display);
                values.Add(value);
            }

            values.Reverse();

            var entry = 0;
            for (var i = 0; i < values.Count; i++)
            {
                entry += values[i] * (int)Math.Pow(10, i);
            }

            totalSum += entry;

            string Construct(params int[] positionNumbers)
            {
                Span<char> c = stackalloc char[positionNumbers.Length];
                for (var i = 0; i < positionNumbers.Length; i++)
                    c[i] = positions[positionNumbers[i]];
                InsertionSort(c);
                return new string(c);
            }
        }

        return new ValueTask<string>(totalSum.ToString());
    }

    private static string SortString(string s)
    {
        Span<char> c = stackalloc char[s.Length];
        for (var i = 0; i < s.Length; i++)
            c[i] = s[i];
        InsertionSort(c);
        return new string(c);
    }

    private static void InsertionSort(Span<char> arr)
    {
        for (var i = 1; i < arr.Length; i++)
        {
            var key = arr[i];
            var j = i - 1;

            while (j >= 0 && arr[j] > key)
            {
                arr[j + 1] = arr[j];
                j = j - 1;
            }

            arr[j + 1] = key;
        }
    }

    private static (string[] digits, string[] displays) ParseLine(string line)
    {
        var dataSplit = line.Split('|');
        var digits = dataSplit[0].Split(' ', StringSplitOptions.RemoveEmptyEntries);
        var displays = dataSplit[1].Split(' ', StringSplitOptions.RemoveEmptyEntries);
        for (var i = 0; i < digits.Length; i++)
        {
            digits[i] = SortString(digits[i]);
        }

        for (var i = 0; i < displays.Length; i++)
        {
            displays[i] = SortString(displays[i]);
        }

        return (digits, displays);
    }

    private static int IndexOf<T>(T[] collection, Predicate<T> predicate)
    {
        for (var index = 0; index < collection.Length; index++)
        {
            var c = collection[index];
            if (predicate(c))
                return index;
        }

        throw new KeyNotFoundException();
    }
}