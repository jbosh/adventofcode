﻿namespace Year2021;

public class Day10 : BaseDay
{
    private string[] lines;

    public override ValueTask<string> Solve_1()
    {
        this.lines = File.ReadAllLines("data/day10.txt");

        var score = 0L;
        var stack = new Stack<char>(256);
        for (var lineIndex = 0; lineIndex < this.lines.Length; lineIndex++)
        {
            var line = this.lines[lineIndex];
            stack.Clear();
            for (var charIndex = 0; charIndex < line.Length; charIndex++)
            {
                var c = line[charIndex];
                switch (c)
                {
                    case '{':
                    case '[':
                    case '(':
                    case '<':
                    {
                        stack.Push(c);
                        break;
                    }
                    case '}':
                    case ']':
                    case ')':
                    case '>':
                    {
                        var top = stack.Pop();
                        var expectedChar = top switch
                        {
                            '{' => '}',
                            '[' => ']',
                            '(' => ')',
                            '<' => '>',
                            _ => throw new KeyNotFoundException(top.ToString()),
                        };
                        if (expectedChar != c)
                        {
                            score += c switch
                            {
                                ')' => 3,
                                ']' => 57,
                                '}' => 1197,
                                '>' => 25137,
                                _ => throw new NotImplementedException(),
                            };
                        }

                        break;
                    }
                    default:
                    {
                        throw new NotImplementedException();
                    }
                }
            }
        }

        return new ValueTask<string>(score.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
#if DEBUG // can re-use
        this.lines = File.ReadAllLines("data/day10.txt");
#endif

        var scores = new List<long>(this.lines.Length);
        var stack = new Stack<char>(256);

        for (var lineIndex = 0; lineIndex < this.lines.Length; lineIndex++)
        {
            var corrupted = false;
            var line = this.lines[lineIndex];
            for (var charIndex = 0; charIndex < line.Length && !corrupted; charIndex++)
            {
                var c = line[charIndex];
                switch (c)
                {
                    case '{':
                    case '[':
                    case '(':
                    case '<':
                    {
                        stack.Push(c);
                        break;
                    }
                    case '}':
                    case ']':
                    case ')':
                    case '>':
                    {
                        var top = stack.Pop();
                        var expectedChar = top switch
                        {
                            '{' => '}',
                            '[' => ']',
                            '(' => ')',
                            '<' => '>',
                            _ => throw new KeyNotFoundException(top.ToString()),
                        };
                        if (expectedChar != c)
                        {
                            corrupted = true;
                            stack.Clear();
                        }

                        break;
                    }
                    default:
                    {
                        throw new NotImplementedException();
                    }
                }
            }

            if (stack.Count != 0)
            {
                var score = 0L;
                while (stack.TryPop(out var c))
                {
                    score *= 5;
                    score += c switch
                    {
                        '(' => 1,
                        '[' => 2,
                        '{' => 3,
                        '<' => 4,
                        _ => throw new NotImplementedException(),
                    };
                }

                scores.Add(score);
            }
        }

        scores.Sort();
        var output = scores[scores.Count / 2];
        return new ValueTask<string>(output.ToString());
    }
}