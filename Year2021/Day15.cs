﻿namespace Year2021;

public class Day15 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var grid = Grid.FromFile("data/day15.txt");

        var score = Search(grid, new Point(0, 0), new Point(grid.Width - 1, grid.Height - 1));

        return new ValueTask<string>(score.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var originalGrid = Grid.FromFile("data/day15.txt");
        var grid = new Grid(originalGrid.Width * 5, originalGrid.Height * 5);
        for (var x = 0; x < originalGrid.Width; x++)
        {
            for (var y = 0; y < originalGrid.Height; y++)
            {
                var value = originalGrid[x, y] - 1;
                for (var scalarX = 0; scalarX < 5; scalarX++)
                {
                    for (var scalarY = 0; scalarY < 5; scalarY++)
                    {
                        var scaledValue = value + scalarX + scalarY;
                        scaledValue = scaledValue % 9;
                        scaledValue++;
                        grid[x + (scalarX * originalGrid.Width), y + (scalarY * originalGrid.Height)] = scaledValue;
                    }
                }
            }
        }

        var score = Search(grid, new Point(0, 0), new Point(grid.Width - 1, grid.Height - 1));
        return new ValueTask<string>(score.ToString());
    }

    private record struct QueueNode(int x, int y, int cost);

    public static int Search(Grid grid, Point start, Point end)
    {
        // Use dijkstra with some specific costs that are always less than 10. It's not going to be efficient
        // on more diverse grids but for this one, it gets us some very low times.
        var width = grid.Width;
        var height = grid.Height;
        var visited = new bool[width * height];
        var queues = new Stack<QueueNode>[10];
        for (var i = 0; i < queues.Length; i++)
            queues[i] = new Stack<QueueNode>(width * height);

        queues[0].Push(new QueueNode(0, 0, 0));

        while (true)
        {
            foreach (var queue in queues)
            {
                while (queue.TryPop(out var current))
                {
                    var (x, y, cost) = current;
                    if (x == end.X && y == end.Y)
                        return cost;

                    if (visited[(y * height) + x])
                        continue;

                    visited[(y * height) + x] = true;

                    if (x < width - 1)
                        ProcessNode(cost, x + 1, y);
                    if (x != 0)
                        ProcessNode(cost, x - 1, y);
                    if (y < height - 1)
                        ProcessNode(cost, x, y + 1);
                    if (y != 0)
                        ProcessNode(cost, x, y - 1);
                }
            }
        }

        void ProcessNode(int cost, int x, int y)
        {
            if (visited[(y * height) + x])
                return;

            var weight = grid[x, y];
            var newCost = weight + cost;

            queues[newCost % 10].Push(new QueueNode(x, y, newCost));
        }
    }
}