﻿using System.Diagnostics;

namespace Year2021;

public class Day18 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var numbers = File.ReadAllLines("data/day18.txt")
            .Select(Parser.Parse)
            .ToArray();

        var result = numbers[0];
        for (var i = 1; i < numbers.Length; i++)
            result = Number.Add(result, numbers[i]);

        var magnitude = Number.Magnitude(result);
        return new ValueTask<string>(magnitude.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var numbers = File.ReadAllLines("data/day18.txt")
            .Select(Parser.Parse)
            .ToArray();

        var largest = 0L;
        for (var i = 0; i < numbers.Length; i++)
        {
            for (var j = 0; j < numbers.Length; j++)
            {
                if (i == j)
                    continue;

                var result = Number.Add(Number.Clone(numbers[i]), numbers[j]);
                var magnitude = Number.Magnitude(result);
                if (magnitude > largest)
                {
                    largest = magnitude;
                }
            }
        }

        return new ValueTask<string>(largest.ToString());
    }

    public class NumberTreeNode
    {
        public NumberTreeNode Left;
        public NumberTreeNode Right;
        public int Value;
        public int Depth;
        public bool IsNumber => this.Left == null;
        public override string ToString() => this.IsNumber ? $"{this.Value}" : $"[{this.Left},{this.Right}]";

        public NumberTreeNode(int depth, int value)
        {
            this.Depth = depth;
            this.Value = value;
        }

        public NumberTreeNode(int depth, NumberTreeNode left, NumberTreeNode right)
        {
            this.Depth = depth;
            this.Left = left;
            this.Right = right;
            Debug.Assert((left == null) == (right == null), "Left and right should be either null or non-null.");
        }
    }

    public struct Number
    {
        public int Depth;
        public int Value;
        public override string ToString() => $"Value = {this.Value}, Depth = {this.Depth}";

        public Number(NumberTreeNode node)
        {
            Debug.Assert(node.IsNumber, "Number must be IsNumber.");

            this.Depth = node.Depth;
            this.Value = node.Value;
        }

        public Number(int value, int depth)
        {
            this.Depth = depth;
            this.Value = value;
        }

        public static LinkedList<Number> Clone(LinkedList<Number> list)
        {
            var result = new LinkedList<Number>();

            for (var node = list.First; node != null; node = node.Next)
            {
                result.AddLast(node.Value);
            }

            return result;
        }

        public static LinkedList<Number> Add(LinkedList<Number> a, LinkedList<Number> b)
        {
            var reducing = false;
            for (var node = a.First; node != null; node = node.Next)
            {
                var number = node.Value;
                number.Depth++;
                node.Value = number;
                if (number.Depth > 4)
                    reducing = true;
            }

            var result = a;
            for (var node = b.First; node != null; node = node.Next)
            {
                var number = node.Value;
                number.Depth++;
                result.AddLast(number);
                if (number.Depth > 4)
                    reducing = true;
            }

            while (reducing)
            {
                reducing = false;
                reducing |= Explode(result);
                reducing |= Split(result);
            }

            return result;
        }

        public static long Magnitude(LinkedList<Number> list)
        {
            while (list.First.Next != null)
            {
                for (var node = list.First; ; node = node.Next)
                {
                    if (node.Next == null)
                        break;

                    var left = node.Value;
                    var right = node.Next.Value;
                    if (left.Depth != right.Depth)
                        continue;

                    var value = (left.Value * 3) + (right.Value * 2);
                    var depth = left.Depth - 1;
                    list.Remove(node.Next);
                    node.Value = new Number(value, depth);
                    break;
                }
            }

            return list.First.Value.Value;
        }

        private static bool Explode(LinkedList<Number> list)
        {
            var modified = false;
            var exploding = true;
            while (exploding)
            {
                exploding = false;
                for (var node = list.First; node != null; node = node.Next)
                {
                    var number = node.Value;
                    if (number.Depth <= 4)
                        continue;

                    var next = node.Next;
                    var left = number;
                    var right = next.Value;
                    Debug.Assert(left.Depth == right.Depth, "Mismatched depths.");
                    if (node.Previous != null)
                    {
                        var add = node.Previous.Value;
                        add.Value += left.Value;
                        node.Previous.Value = add;
                    }

                    if (next.Next != null)
                    {
                        var add = next.Next.Value;
                        add.Value += right.Value;
                        next.Next.Value = add;
                    }

                    list.Remove(next);
                    node.Value = new Number(0, 4);

                    modified = true;
                    exploding = true;
                    break;
                }
            }

            return modified;
        }

        private static bool Split(LinkedList<Number> list)
        {
            var modified = false;
            for (var node = list.First; node != null; node = node.Next)
            {
                var number = node.Value;
                if (number.Value <= 9)
                    continue;

                var left = (number.Value + 0) / 2;
                var right = (number.Value + 1) / 2;

                node.Value = new Number(left, number.Depth + 1);
                list.AddAfter(node, new Number(right, number.Depth + 1));
                modified = true;
                break;
            }

            return modified;
        }
    }

    public enum TokenType
    {
        LBrace,
        RBrace,
        Comma,
        Digit,
    }

    public readonly struct Token
    {
        public readonly TokenType Type;
        public readonly string Value;

        public override string ToString()
        {
            return this.Value ?? $"{this.Type}";
        }

        public Token(TokenType type)
        {
            this.Type = type;
            this.Value = null;
        }

        public Token(TokenType type, string value)
        {
            this.Type = type;
            this.Value = value;
        }
    }

    public static class Parser
    {
        public static IEnumerable<Token> Tokenize(string text)
        {
            for (var i = 0; i < text.Length; i++)
            {
                var c = text[i];
                switch (c)
                {
                    case ',':
                    {
                        yield return new Token(TokenType.Comma);
                        break;
                    }
                    case '[':
                    {
                        yield return new Token(TokenType.LBrace);
                        break;
                    }
                    case ']':
                    {
                        yield return new Token(TokenType.RBrace);
                        break;
                    }
                    default:
                    {
                        var startIndex = i;
                        for (; i < text.Length; i++)
                        {
                            c = text[i];
                            if (c < '0' || c > '9')
                                break;
                        }

                        i--;
                        yield return new Token(TokenType.Digit, text.Substring(startIndex, i - startIndex + 1));

                        break;
                    }
                }
            }
        }

        public static LinkedList<Number> Parse(string text)
        {
            var tokens = new Queue<Token>(Parser.Tokenize(text));

            var number = Expression(0, tokens);
            var list = new LinkedList<Number>();
            BuildTree(list, number);
            return list;
        }

        private static void BuildTree(LinkedList<Number> list, NumberTreeNode number)
        {
            if (number.IsNumber)
            {
                list.AddLast(new Number(number));
            }
            else
            {
                BuildTree(list, number.Left);
                BuildTree(list, number.Right);
            }
        }

        private static NumberTreeNode Expression(int depth, Queue<Token> tokens)
        {
            var peek = tokens.Peek();
            switch (peek.Type)
            {
                case TokenType.Digit:
                {
                    var left = new NumberTreeNode(depth, int.Parse(tokens.Dequeue().Value));
                    if (tokens.Peek().Type != TokenType.Comma)
                        return left;

                    tokens.Dequeue(); // comma
                    var right = Expression(depth, tokens);
                    return new NumberTreeNode(depth, left, right);
                }
                case TokenType.LBrace:
                {
                    tokens.Dequeue();
                    var left = Expression(depth + 1, tokens);
                    var type = tokens.Dequeue().Type;
                    if (type == TokenType.RBrace)
                        return left;

                    Debug.Assert(type == TokenType.Comma, "Syntax error.");
                    var right = Expression(depth + 1, tokens);
                    type = tokens.Dequeue().Type;
                    Debug.Assert(type == TokenType.RBrace, "Syntax error.");
                    return new NumberTreeNode(depth, left, right);
                }
                default:
                {
                    throw new NotImplementedException();
                }
            }
        }
    }
}