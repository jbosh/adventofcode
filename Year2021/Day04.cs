﻿namespace Year2021;

public class Day04 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day04.txt");

        var numbers = lines[0].Split(',').Select(int.Parse).ToArray();

        var boards = new List<Board>();
        for (var lineIndex = 2; lineIndex < lines.Length; lineIndex += 6)
        {
            var board = new Board(lineIndex, lines);
            boards.Add(board);
        }

        foreach (var number in numbers)
        {
            foreach (var board in boards)
            {
                board.Call(number);
                if (board.Won)
                {
                    var score = board.Score;
                    var output = score * number;
                    return new ValueTask<string>(output.ToString());
                }
            }
        }

        throw new Exception("Failed to solve puzzle.");
    }

    public override ValueTask<string> Solve_2()
    {
        var lines = File.ReadAllLines("data/day04.txt");

        var numbers = lines[0].Split(',').Select(int.Parse).ToArray();

        var boards = new List<Board>();
        for (var lineIndex = 2; lineIndex < lines.Length; lineIndex += 6)
        {
            var board = new Board(lineIndex, lines);
            boards.Add(board);
        }

        for (var numberIndex = 0; numberIndex < numbers.Length; numberIndex++)
        {
            var number = numbers[numberIndex];
            var numberOfWinningBoards = 0;
            foreach (var board in boards)
            {
                board.Call(number);
                if (board.Won)
                    numberOfWinningBoards++;
            }

            if (numberOfWinningBoards == boards.Count - 1)
            {
                // Need to keep going until this board wins
                var board = boards.First(b => !b.Won);
                for (; numberIndex < numbers.Length; numberIndex++)
                {
                    number = numbers[numberIndex];
                    board.Call(number);
                    if (board.Won)
                    {
                        var score = board.Score;
                        var output = score * number;
                        return new ValueTask<string>(output.ToString());
                    }
                }
            }
        }

        throw new Exception("Failed to solve puzzle.");
    }

    private sealed class Board
    {
        private readonly int[,] numbers;
        private readonly bool[,] called;
        public bool Won { get; private set; }

        public Board(int index, string[] lines)
        {
            this.numbers = new int[5, 5];
            for (var y = 0; y < 5; y++)
            {
                var columns = lines[index + y].Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
                for (var x = 0; x < 5; x++)
                    this.numbers[x, y] = columns[x];
            }

            this.called = new bool[5, 5];
        }

        public void Call(int number)
        {
            for (var x = 0; x < 5; x++)
            {
                for (var y = 0; y < 5; y++)
                {
                    if (this.numbers[x, y] == number)
                        this.called[x, y] = true;
                }
            }

            // Check columns
            for (var x = 0; x < 5; x++)
            {
                var allCalled = true;
                for (var y = 0; y < 5 && allCalled; y++)
                {
                    if (!this.called[x, y])
                        allCalled = false;
                }

                if (allCalled)
                    this.Won = true;
            }

            // Check rows
            for (var y = 0; y < 5; y++)
            {
                var allCalled = true;
                for (var x = 0; x < 5 && allCalled; x++)
                {
                    if (!this.called[x, y])
                        allCalled = false;
                }

                if (allCalled)
                    this.Won = true;
            }
        }

        public int Score
        {
            get
            {
                var sum = 0;
                for (var x = 0; x < 5; x++)
                {
                    for (var y = 0; y < 5; y++)
                    {
                        if (!this.called[x, y])
                        {
                            sum += this.numbers[x, y];
                        }
                    }
                }

                return sum;
            }
        }
    }
}