﻿namespace Year2021;

public class Day01 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day01.txt")
            .Select(int.Parse)
            .ToArray();

        var count = 0;
        for (var i = 1; i < lines.Length; i++)
        {
            var prev = lines[i - 1];
            var value = lines[i];
            if (prev < value)
                count++;
        }

        return new ValueTask<string>(count.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var lines = File.ReadAllLines("data/day01.txt")
            .Select(int.Parse)
            .ToArray();

        var count = 0;
        for (var i = 3; i < lines.Length; i++)
        {
            var prev = lines[i - 1] + lines[i - 2] + lines[i - 3];
            var value = lines[i] + lines[i - 1] + lines[i - 2];
            if (prev < value)
                count++;
        }

        return new ValueTask<string>(count.ToString());
    }
}