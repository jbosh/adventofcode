﻿namespace Year2021;

public class Day05 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day05.txt")
            .Select(Line.FromText)
            .ToArray();

        var maxX = 0;
        var maxY = 0;
        foreach (var line in lines)
        {
            maxX = Math.Max(maxX, Math.Max(line.X0, line.X1));
            maxY = Math.Max(maxX, Math.Max(line.X0, line.X1));
        }

        maxX++;
        maxY++;

        var grid = new byte[maxX, maxY];

        var dangerCount = 0L;
        foreach (var line in lines)
        {
            if (line.IsDiagonal)
                continue;

            for (var x = line.X0; x <= line.X1; x++)
            {
                for (var y = line.Y0; y <= line.Y1; y++)
                {
                    var value = grid[x, y];
                    if (value == 0)
                    {
                        grid[x, y] = 1;
                    }
                    else if (value == 1)
                    {
                        grid[x, y] = 2;
                        dangerCount++;
                    }
                }
            }
        }

        return new ValueTask<string>(dangerCount.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var lines = File.ReadAllLines("data/day05.txt")
            .Select(Line.FromText)
            .ToArray();

        var maxX = 0;
        var maxY = 0;
        foreach (var line in lines)
        {
            maxX = Math.Max(maxX, Math.Max(line.X0, line.X1));
            maxY = Math.Max(maxX, Math.Max(line.X0, line.X1));
        }

        maxX++;
        maxY++;

        var grid = new byte[maxX, maxY];

        var dangerCount = 0L;
        foreach (var line in lines)
        {
            if (line.IsDiagonal)
            {
                var count = line.X1 - line.X0;
                var up = line.Y0 > line.Y1 ? -1 : 1;
                for (var index = 0; index <= count; index++)
                {
                    var x = line.X0 + index;
                    var y = line.Y0 + (index * up);
                    var value = grid[x, y];
                    if (value == 0)
                    {
                        grid[x, y] = 1;
                    }
                    else if (value == 1)
                    {
                        grid[x, y] = 2;
                        dangerCount++;
                    }
                }
            }
            else
            {
                for (var x = line.X0; x <= line.X1; x++)
                {
                    for (var y = line.Y0; y <= line.Y1; y++)
                    {
                        var value = grid[x, y];
                        if (value == 0)
                        {
                            grid[x, y] = 1;
                        }
                        else if (value == 1)
                        {
                            grid[x, y] = 2;
                            dangerCount++;
                        }
                    }
                }
            }
        }

        return new ValueTask<string>(dangerCount.ToString());
    }

    private struct Line
    {
        public readonly int X0;
        public readonly int Y0;
        public readonly int X1;
        public readonly int Y1;

        public bool IsDiagonal { get; }

        public override string ToString() => $"{this.X0},{this.Y0} -> {this.X1},{this.Y1}";

        public Line(int x0, int y0, int x1, int y1)
        {
            var isDiagonal = x0 != x1 && y0 != y1;
            if (isDiagonal)
            {
                if (x0 < x1)
                {
                    this.X0 = x0;
                    this.Y0 = y0;
                    this.X1 = x1;
                    this.Y1 = y1;
                }
                else
                {
                    this.X0 = x1;
                    this.Y0 = y1;
                    this.X1 = x0;
                    this.Y1 = y0;
                }
            }
            else
            {
                this.X0 = Math.Min(x0, x1);
                this.Y0 = Math.Min(y0, y1);
                this.X1 = Math.Max(x0, x1);
                this.Y1 = Math.Max(y0, y1);
            }

            this.IsDiagonal = isDiagonal;
        }

        public static Line FromText(string text)
        {
            var points = text.Split(" -> ", 2);
            var point0 = points[0].Split(',', 2);
            var point1 = points[1].Split(',', 2);

            var x0 = int.Parse(point0[0]);
            var y0 = int.Parse(point0[1]);
            var x1 = int.Parse(point1[0]);
            var y1 = int.Parse(point1[1]);
            return new Line(x0, y0, x1, y1);
        }
    }
}