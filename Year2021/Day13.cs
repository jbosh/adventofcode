﻿namespace Year2021;

public class Day13 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        FoldInfo.FromFile("data/day13.txt", out var points, out var width, out var height, out var folds);

        ApplyFolds(ref width, ref height, points, folds.Take(1));

        var output = 0L;
        var grid = new BoolGrid(width, height);
        foreach (var pt in points)
        {
            if (grid[pt.X, pt.Y])
                continue;

            output++;
            grid[pt.X, pt.Y] = true;
        }

        return new ValueTask<string>(output.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        FoldInfo.FromFile("data/day13.txt", out var points, out var width, out var height, out var folds);

        ApplyFolds(ref width, ref height, points, folds);

        var grid = new BoolGrid(width, height);
        foreach (var pt in points)
        {
            if (grid[pt.X, pt.Y])
                continue;

            grid[pt.X, pt.Y] = true;
        }

#if false // Prints the solution. Not here because it makes output weird.
        Console.WriteLine();
        grid.Print(width, height);
#endif

        return new ValueTask<string>("LRFJBJEH");
    }

    private static void ApplyFolds(ref int width, ref int height, Point[] points, IEnumerable<FoldInfo.Fold> folds)
    {
        foreach (var fold in folds)
        {
            var foldValue = fold.Value;
            if (fold.IsXAxis)
            {
                for (var i = 0; i < points.Length; i++)
                {
                    var pt = points[i];
                    if (pt.X > foldValue)
                    {
                        var newX = foldValue - pt.X + foldValue;
                        points[i] = new Point(newX, pt.Y);
                    }
                }

                width = foldValue;
            }
            else
            {
                for (var i = 0; i < points.Length; i++)
                {
                    var pt = points[i];
                    if (pt.Y > foldValue)
                    {
                        var newY = foldValue - pt.Y + foldValue;
                        points[i] = new Point(pt.X, newY);
                    }
                }

                height = foldValue;
            }
        }
    }

    private static class FoldInfo
    {
        public readonly struct Fold
        {
            public readonly bool IsXAxis;
            public readonly int Value;

            public Fold(bool isXAxis, int value)
            {
                this.IsXAxis = isXAxis;
                this.Value = value;
            }
        }

        public static void FromFile(string path, out Point[] points, out int width, out int height, out Fold[] folds)
        {
            var lines = File.ReadAllLines(path);
            var lineIndex = 0;
            var pointList = new List<Point>(lines.Length);
            for (; lineIndex < lines.Length; lineIndex++)
            {
                var line = lines[lineIndex];
                if (line == string.Empty)
                {
                    lineIndex++;
                    break;
                }

                var point = Point.Parse(line);
                pointList.Add(point);
            }

            var foldList = new List<Fold>(lines.Length - lineIndex);

            for (; lineIndex < lines.Length; lineIndex++)
            {
                var line = lines[lineIndex];
                var axis = line["fold along ".Length];
                var value = int.Parse(line.Substring("fold along ?=".Length));
                foldList.Add(new Fold(axis == 'x', value));
            }

            width = pointList.Max(p => p.X) + 1;
            height = pointList.Max(p => p.Y) + 1;

            folds = foldList.ToArray();
            points = pointList.ToArray();
        }
    }
}