﻿namespace Year2021;

public class Day09 : BaseDay
{
    private int[,] cells;
    private int width;
    private int height;
    private (int x, int y)[] lowPoints;

    public override ValueTask<string> Solve_1()
    {
        this.LoadPoints("data/day09.txt");
        this.lowPoints = this.GetLowPoints();

        var output = this.lowPoints.Sum(p => this.GetHeight(p.x, p.y) + 1);
        return new ValueTask<string>(output.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
#if DEBUG // This data is re-usable.
        this.LoadPoints("data/day09.txt");
        this.lowPoints = this.GetLowPoints();
#endif

        var basins = new bool[this.width * this.height];
        for (var x = 0; x < this.width; x++)
        {
            for (var y = 0; y < this.height; y++)
            {
                if (this.GetHeight(x, y) == 9)
                    basins[(y * this.height) + x] = true;
            }
        }

        var reverseCompare = new GenericComparer<int>((x, y) => y.CompareTo(x));
        var resultList = new List<int>();
        foreach (var (x, y) in this.lowPoints)
        {
            var count = this.Fill(x, y, basins);
            var index = resultList.BinarySearch(count, reverseCompare);
            if (index < 0)
                resultList.Insert(~index, count);
            else
                resultList.Insert(index, count);
            if (resultList.Count > 3)
                resultList.RemoveAt(3);
        }

        var result = resultList.Aggregate(1L, (current, value) => current * value);

        return new ValueTask<string>(result.ToString());
    }

    private int Fill(int x, int y, bool[] basins)
    {
        if (x < 0 || x >= this.width || y < 0 || y >= this.height)
            return 0;

        if (basins[(y * this.height) + x])
            return 0;

        basins[(y * this.height) + x] = true;
        return this.Fill(x - 1, y, basins)
            + this.Fill(x + 1, y, basins)
            + this.Fill(x, y - 1, basins)
            + this.Fill(x, y + 1, basins)
            + 1;
    }

    private void LoadPoints(string path)
    {
        var lines = File.ReadAllLines(path);
        this.height = lines.Length;
        this.width = lines[0].Length;

        var cells = new int[this.width, this.height];
        for (var x = 0; x < this.width; x++)
        {
            for (var y = 0; y < this.height; y++)
            {
                var value = lines[y][x] - '0';
                cells[x, y] = value;
            }
        }

        this.cells = cells;
    }

    private static IEnumerable<T> FlattenArray<T>(T[,] collection)
    {
        var width = collection.GetLength(0);
        var height = collection.GetLength(1);
        for (var x = 0; x < width; x++)
        {
            for (var y = 0; y < height; y++)
                yield return collection[x, y];
        }
    }

    private (int x, int y)[] GetLowPoints()
    {
        var lowPoints = new List<(int x, int y)>();
        for (var x = 0; x < this.width; x++)
        {
            for (var y = 0; y < this.height; y++)
            {
                var isLowest = this.IsLowest(x, y);
                if (isLowest)
                {
                    lowPoints.Add((x, y));
                }
            }
        }

        return lowPoints.ToArray();
    }

    private int GetHeight(int x, int y)
    {
        if (x < 0 || x >= this.width || y < 0 || y >= this.height)
            return 10;
        return this.cells[x, y];
    }

    private bool IsLowest(int x, int y)
    {
        var height = this.GetHeight(x, y);
        for (var i = x - 1; i <= x + 1; i++)
        {
            for (var j = y - 1; j <= y + 1; j++)
            {
                var compare = this.GetHeight(i, j);
                if (i == x && j == y)
                    continue;
                if (height >= compare)
                    return false;
            }
        }

        return true;
    }
}