﻿namespace Year2021;

public class Day06 : BaseDay
{
    public override ValueTask<string> Solve_1() => Puzzle(80);
    public override ValueTask<string> Solve_2() => Puzzle(256);

    private static ValueTask<string> Puzzle(int numberOfDays)
    {
        var initialState = File.ReadAllText("data/day06.txt")
            .Split(',')
            .Select(int.Parse)
            .ToArray();

        var cohorts = new long[9];
        foreach (var days in initialState)
            cohorts[days]++;

        for (var day = 1; day <= numberOfDays; day++)
        {
            var newLanternfish = cohorts[0]; // cohort 0 becomes 6 and spawns

            for (var i = 1; i < 9; i++)
            {
                cohorts[i - 1] = cohorts[i];
            }

            // Restart cycle for to 6 for fish0
            cohorts[6] += newLanternfish;

            // Newly spawned fish
            cohorts[8] = newLanternfish;
        }

        var output = cohorts.Sum();
        return new ValueTask<string>(output.ToString());
    }
}