﻿using System.Diagnostics;

namespace Year2021;

public class Day19 : BaseDay
{
    private static readonly List<Vector3> Centers = new();

    public override ValueTask<string> Solve_1()
    {
        var lines = File.ReadAllLines("data/day19.txt");
        var scanners = new List<List<Vector3>>();
        var signatures = new List<List<SortedList<int>>>();

        // Parse
        var currentScanner = default(List<Vector3>);
        Debug.Assert(lines[0].Length == 0, "First line should be empty.");
        for (var i = 0; i < lines.Length; i++)
        {
            var line = lines[i];
            if (line.Length == 0 || currentScanner == null)
            {
                i++; // skip this line
                currentScanner = new List<Vector3>();
                scanners.Add(currentScanner);
            }
            else
            {
                currentScanner.Add(Vector3.Parse(line));
            }
        }

        // Create sets of satellite signatures
        for (var i = 0; i < scanners.Count; i++)
        {
            var scanner = scanners[i];
            var list = new List<SortedList<int>>();
            for (var j = 0; j < scanner.Count; j++)
            {
                var set = new SortedList<int>(scanner.Count);
                for (var k = 0; k < scanner.Count; k++)
                {
                    if (j == k)
                        continue;

                    var dist = Vector3.DistSq(scanner[j], scanner[k]);
                    set.Add(dist);
                }

                list.Add(set);
            }

            signatures.Add(list);
        }

        var allSatellites = new HashSet<Vector3>(scanners[0]);
        Centers.Clear();
        Centers.Add(Vector3.Zero);

        // Search for matching signatures
        var mainSatellites = scanners[0];
        var mainSatelliteSignatures = signatures[0];
        scanners.RemoveAt(0);
        signatures.RemoveAt(0);

        var matches = new (Vector3, Vector3)[2];

        var lastSatelliteCount = 0;
        var rollingCounts = new List<int>();
        for (var i = 0; i < signatures.Count; i++)
            rollingCounts.Add(0);

        var mutated = true;
        while (mutated)
        {
            mutated = false;
            var startSatelliteCount = lastSatelliteCount;
            lastSatelliteCount = mainSatellites.Count;
            Debug.Assert(signatures.Count == scanners.Count, "Didn't find all signatures/scanners.");
            for (var signatureIndex = 0; signatureIndex < signatures.Count; signatureIndex++)
            {
                var signatureList = signatures[signatureIndex];

                var matchCount = rollingCounts[signatureIndex];
                for (var i = startSatelliteCount; i < mainSatelliteSignatures.Count; i++)
                {
                    var signature0 = mainSatelliteSignatures[i];
                    for (var j = 0; j < signatureList.Count && matchCount < 2; j++)
                    {
                        var signature1 = signatureList[j];
                        var overlaps = OverlapsEnough(signature0, signature1);
                        if (overlaps)
                        {
                            if (matchCount < matches.Length)
                                matches[matchCount] = (mainSatellites[i], scanners[signatureIndex][j]);
                            matchCount++;
                        }
                    }
                }

                rollingCounts[signatureIndex] = matchCount;

                if (matchCount >= 2)
                {
                    // We've got several of the same satellites, we're looking at the same set.
                    for (var rotation = 0; rotation < 24; rotation++)
                    {
                        // Don't know center yet, try to get it.
                        var item = Rotate(Vector3.Zero, matches[0].Item2, rotation);
                        var center = matches[0].Item1 - item;

                        var failed = false;
                        for (var i = 1; i < matches.Length && !failed; i++)
                        {
                            item = Rotate(center, matches[i].Item2, rotation);
                            if (matches[i].Item1 != item)
                                failed = true;
                        }

                        if (!failed)
                        {
                            Centers.Add(center);
                            foreach (var beacon in scanners[signatureIndex])
                            {
                                item = Rotate(center, beacon, rotation);
                                if (allSatellites.Add(item))
                                {
                                    var set = new SortedList<int>(mainSatellites.Count);
                                    for (var i = 0; i < mainSatellites.Count; i++)
                                    {
                                        var dist = Vector3.DistSq(mainSatellites[i], item);
                                        set.Add(dist);
                                    }

                                    mainSatelliteSignatures.Add(set);
                                    mainSatellites.Add(item);
                                }
                            }

                            scanners.RemoveAt(signatureIndex);
                            signatures.RemoveAt(signatureIndex);
                            rollingCounts.RemoveAt(signatureIndex);
                            mutated = true;
                            signatureIndex--;
                            break;
                        }
                    }

                    Debug.Assert(mutated, "Shouldn't be able to reach this point.");
                }
            }
        }

        return new ValueTask<string>(mainSatellites.Count.ToString());
    }

    public override ValueTask<string> Solve_2()
    {
        var maxDistance = 0;
        foreach (var center0 in Centers)
        {
            foreach (var center1 in Centers)
            {
                var dist = Math.Abs(center0.X - center1.X)
                    + Math.Abs(center0.Y - center1.Y)
                    + Math.Abs(center0.Z - center1.Z);
                maxDistance = Math.Max(dist, maxDistance);
            }
        }

        return new ValueTask<string>(maxDistance.ToString());
    }

    private static bool OverlapsEnough(SortedList<int> listA, SortedList<int> listB)
    {
        var count = 0;
        var i = 0;
        var j = 0;
        var a = listA[i];
        var b = listB[i];
        while (true)
        {
            if (a == b)
            {
                count++;
                if (count >= 4)
                    return true;
                i++;
                j++;

                if (i >= listA.Count)
                    return false;
                if (j >= listB.Count)
                    return false;
                a = listA[i];
                b = listB[j];
            }
            else if (a > b)
            {
                j++;
                if (j >= listB.Count)
                    return false;
                b = listB[j];
            }
            else
            {
                i++;
                if (i >= listA.Count)
                    return false;
                a = listA[i];
            }
        }
    }

    public static Vector3 Rotate(Vector3 center, Vector3 v, int rotation)
    {
        Debug.Assert(rotation >= 0 && rotation < 24, "Invalid rotation.");

        var x = v.X;
        var y = v.Y;
        var z = v.Z;

#pragma warning disable 1717

        // rotate coordinate system so that x-axis points in the possible 6 directions
        switch (rotation % 6)
        {
            case 0:
                // No rotation necessary.
                break;
            case 1:
                (x, y, z) = (-x, y, -z);
                break;
            case 2:
                (x, y, z) = (y, -x, z);
                break;
            case 3:
                (x, y, z) = (-y, x, z);
                break;
            case 4:
                (x, y, z) = (z, y, -x);
                break;
            case 5:
                (x, y, z) = (-z, y, x);
                break;
        }

        // rotate around x-axis:
        switch ((rotation / 6) % 4)
        {
            case 0:
                // No rotation necessary.
                break;
            case 1:
                (x, y, z) = (x, -z, y);
                break;
            case 2:
                (x, y, z) = (x, -y, -z);
                break;
            case 3:
                (x, y, z) = (x, z, -y);
                break;
        }
#pragma warning restore

        return center + new Vector3(x, y, z);
    }

    public struct Vector3 : IEquatable<Vector3>
    {
        public readonly int X;
        public readonly int Y;
        public readonly int Z;
        public override string ToString() => $"{this.X}, {this.Y}, {this.Z}";
        public static readonly Vector3 Zero = new();

        public Vector3(int x, int y, int z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public static Vector3 Parse(string text)
        {
            var splits = text.Split(',');
            var x = int.Parse(splits[0]);
            var y = int.Parse(splits[1]);
            var z = int.Parse(splits[2]);
            return new Vector3(x, y, z);
        }

        public static int DistSq(Vector3 a, Vector3 b)
        {
            var diff = a - b;
            return (diff.X * diff.X) + (diff.Y * diff.Y) + (diff.Z * diff.Z);
        }

        public static Vector3 Abs(Vector3 v) => new(Math.Abs(v.X), Math.Abs(v.Y), Math.Abs(v.Z));

        public static Vector3 operator -(Vector3 a, Vector3 b) => new(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        public static Vector3 operator +(Vector3 a, Vector3 b) => new(a.X + b.X, a.Y + b.Y, a.Z + b.Z);

        public static bool operator ==(Vector3 a, Vector3 b) => a.Equals(b);
        public static bool operator !=(Vector3 a, Vector3 b) => !a.Equals(b);
        public bool Equals(Vector3 other) => this.X == other.X && this.Y == other.Y && this.Z == other.Z;
        public override bool Equals(object obj) => obj is Vector3 other && this.Equals(other);
        public override int GetHashCode() => this.X ^ this.Y ^ this.Z;
    }
}