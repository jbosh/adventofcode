﻿namespace Year2021;

public class Day07 : BaseDay
{
    public override ValueTask<string> Solve_1()
    {
        var crabs = File.ReadAllText("data/day07.txt")
            .Split(',')
            .Select(int.Parse)
            .OrderBy(i => i)
            .ToArray();

        var middle = crabs.Length / 2;
        var output = ComputeFuelCost(crabs[middle]);
        if (crabs.Length % 2 != 1)
        {
            var output2 = ComputeFuelCost(crabs[middle + 1]);
            return new ValueTask<string>(Math.Min(output, output2).ToString());
        }
        else
        {
            return new ValueTask<string>(output.ToString());
        }

        int ComputeFuelCost(int position)
        {
            return crabs.Sum(i => Math.Abs(i - position));
        }
    }

    public override ValueTask<string> Solve_2()
    {
        var crabs = File.ReadAllText("data/day07.txt")
            .Split(',')
            .Select(int.Parse)
            .ToArray();

        var avg = crabs.Average();

        var lowestFuel = int.MaxValue;
        for (var i = (int)Math.Floor(avg) - 1; i <= (int)Math.Ceiling(avg) + 1; i++)
        {
            var fuel = ComputeFuelCost(i);
            if (fuel < lowestFuel)
            {
                lowestFuel = fuel;
            }
        }

        return new ValueTask<string>(lowestFuel.ToString());

        int ComputeFuelCost(int position)
        {
            return crabs.Sum(i => SumFactorial(Math.Abs(i - position)));
        }

        int SumFactorial(int number)
        {
            if (number == 0)
                return 0;

            var result = 1;
            while (number != 1)
            {
                result += number;
                number--;
            }

            return result;
        }
    }
}